import pytest
from chipcalc.model import *

def test_shape():
    shape = Shape('xx/xx/xx')
    assert shape == Shape({(0, 0), (1, 0), (2, 0),
                           (0, 1), (1, 1), (2, 1)})
    assert shape.width == 2
    assert shape.height == 3
    assert shape
    assert len(shape) == 6

    assert str(shape) == 'xx/xx/xx'

    assert hash(Shape('xx/xx/xx')) == hash(Shape({(0, 0), (1, 0), (2, 0),
                                                  (0, 1), (1, 1), (2, 1)}))

@pytest.mark.parametrize('shape', [Shape(), Shape('xx/xx/xx')])
def test_shape_str(shape):
    assert Shape(str(shape)) == shape

@pytest.mark.parametrize('shape', [Shape(), Shape('xx/xx/xx')])
def test_shape_repr(shape):
    assert eval(repr(shape)) == shape

def test_shape_op():
    shape = Shape('xx/xx/xx')
    assert shape.shift(2, 1) == Shape('//.xx/.xx/.xx')
    assert shape.shift_right() == Shape('.xx/.xx/.xx')
    assert shape.shift_right(2) == Shape('..xx/..xx/..xx')
    assert shape.shift_down() == Shape('/xx/xx/xx')
    assert shape.shift_down(2) == Shape('//xx/xx/xx')
    assert shape.shift(1, 2).shift_normal() == shape

    assert set(shape.translations(Shape('xxx/xxx/xxx/xxx'))) == {
        Shape('xx/xx/xx'),
        Shape('/xx/xx/xx'),
        Shape('.xx/.xx/.xx'),
        Shape('/.xx/.xx/.xx'),
    }

    assert shape.rotate_clockwise() == Shape('xxx/xxx')

    # first item should be unrotated one
    assert list(shape.rotations()) == [
        Shape('xx/xx/xx'),
        Shape('xxx/xxx'),
    ]

def test_chip():
    chip = Chip(Shape('xx/xx/xx'), d=23, p=65, a=20, r=15)

    assert chip.stats['d'] == 23
    assert chip.stats['p'] == 65
    assert chip.stats['a'] == 20
    assert chip.stats['r'] == 15
    assert chip['d'] == 23
    assert chip['p'] == 65
    assert chip['a'] == 20
    assert chip['r'] == 15
    assert chip['no-such-stat'] == 0

    assert str(chip) == 'xx/xx/xx d=23 p=65 a=20 r=15'

def test_chip_zero_stat():
    assert Chip(Shape(), x=0, y=1, z=0) != Chip(Shape(), y=1)

@pytest.mark.parametrize('chip',
                         [Chip(Shape('xx/xx/xx'), d=23, p=65, a=20, r=15),
                          Chip(Shape()),
                          Chip(Shape('x'), {'a=b': 1}),
                         ])
def test_chip_str(chip):
    assert Chip.parse(str(chip)) == chip

@pytest.mark.parametrize('chip',
                         [Chip(Shape('xx/xx/xx'), d=23, p=65, a=20, r=15),
                          Chip(Shape('xxx/xxx')),
                          Chip(Shape(), {'-x-': 1}),
                          Chip(Shape('x'), {'shape': 1}),
                          Chip(Shape('x'), {'stats': 1}),
                          Chip(Shape('xx/xx/xx'), {'if': 1}),
                          Chip(Shape('xx/xx/xx'), {'True': 1}),
                         ])
def test_chip_repr(chip):
    assert eval(repr(chip)) == chip

def test_parameter():
    area = Shape('xxx/xxx/xxx')
    chips = [Chip(Shape('xx/xx'), x=10)]
    stats = {'x': (10, 20), '__len__': (4, None), '__rotate__': (None, 1)}

    param = Parameter(area, chips, stats)

    assert param.area == area
    assert param.chips == chips
    assert param.stats == stats

def test_solution():
    chips = [Chip(Shape('xxxxxx'), x=10),
             Chip(Shape('xxxxxx'), x=20, y=50),
             Chip(Shape('xxxxxx'), y=10)]

    sol = Solution([SolutionEntry(0, 0, 0, 0),
                    SolutionEntry(1, 0, 1, 0)])

    assert sol.chips(chips) == [Chip(Shape('xxxxxx'), x=10),
                                Chip(Shape('/xxxxxx'), x=20, y=50)]
    assert sol.chip_ids() == [0, 1]
    assert sol.stats(chips) == {'x': 30, 'y': 50}
    assert sol.get_stat('x', chips) == 30
    assert sol.get_stat('y', chips) == 50
    assert sol.get_stat('__len__', chips) == 12
    assert sol.get_stat('__rotate__', chips) == 0
