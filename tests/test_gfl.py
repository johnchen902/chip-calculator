import dataclasses

import pytest

from chipcalc.model import Shape, Chip
import chipcalc.gfl as gfl

@pytest.mark.parametrize('shape,stars,density', [
    ('xxxxxx', 5, 1),
    ('xxxxxx', 4, 0.8),
    ('xxxxxx', 3, 0.6),
    ('xxxxxx', 2, 0.4),
    ('x/xxx/x', 5, 1),
    ('x/xxx/x', 4, 0.8),
    ('x/xxx/x', 3, 0.6),
    ('x/xxx/x', 2, 0.4),
    ('xxxxx', 5, 0.92),
    ('xxxxx', 4, 0.72),
    ('xxxxx', 3, 0.56),
    ('xxxxx', 2, 0.36),
    ('xxxx', 5, 0.8),
    ('xxxx', 4, 0.64),
    ('xxxx', 3, 0.48),
    ('xxxx', 2, 0.32),
    ('xxx', 5, 0.76),
    ('xxx', 4, 0.6),
    ('xxx', 3, 0.48),
    ('xxx', 2, 0.32),
])
def test_get_shape_density(shape, stars, density):
    assert gfl.get_shape_density(Shape(shape), stars) == density

@pytest.mark.parametrize('shape,stars', [
    ('xxxxxx', 1),
    ('xxxxxx', 6),
    ('xx', 5),
    ('xxxxxxx', 5),
])
def test_get_bad_shape_density(shape, stars):
    with pytest.raises(ValueError):
        gfl.get_shape_density(Shape(shape), stars)

def test_get_level_multiplier():
    assert gfl.get_level_multiplier(0) == 1.0
    assert gfl.get_level_multiplier(10) == 1.8
    assert gfl.get_level_multiplier(11) == 1.87
    assert gfl.get_level_multiplier(20) == 2.5

def test_get_possible_full_chips():
    # not a strong test but at least there are no error
    chip = Chip(Shape('xx/xx/xx'), d=9, p=13, a=8, r=12)
    full_chip = Chip(Shape('xx/xx/xx'), d=9, p=13, a=8, r=12,
                     db=2, pb=1, ab=1, rb=2, stars=5, level=0)
    assert gfl.get_possible_full_chips(chip) == [full_chip]

@pytest.mark.parametrize('level', list(range(0, 21)))
@pytest.mark.parametrize('stars', list(range(2, 6)))
@pytest.mark.parametrize('chip', [
    Chip(Shape('xx/xx/xx'), db=2, pb=1, ab=1, rb=2),
    Chip(Shape('x/xxx/x'), db=2, ab=1, rb=2),
    Chip(Shape('xxxxx'), db=2, ab=1, rb=2),
    Chip(Shape('xxxx'), db=2, ab=1, rb=1),
    Chip(Shape('xxx'), db=1, ab=1, rb=1),
])
def test_round_trip(chip, stars, level):
    chip = dataclasses.replace(chip, stars=stars)

    chip_with_level = gfl.with_level(chip, level)

    chip_back = gfl.fill_blocks(chip_with_level)

    assert chip['db'] == chip_back['db']
    assert chip['pb'] == chip_back['pb']
    assert chip['ab'] == chip_back['ab']
    assert chip['rb'] == chip_back['rb']
