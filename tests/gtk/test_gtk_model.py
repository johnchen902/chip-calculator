import collections.abc

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GObject, Gtk

import pytest

from chipcalc.model import Shape, Chip, Parameter, SolutionEntry, Solution
import chipcalc.gtk.model as gtkmodel

def test_gtk_model():
    model = gtkmodel.AppModel()

    model.area = Shape('xxx')
    assert model.area == Shape('xxx')

    with pytest.raises(TypeError, match='read-only|not writable'):
        model.stats = []

    assert isinstance(model.stats_map, collections.abc.MutableMapping)
    with pytest.raises(TypeError, match='read-only|not writable'):
        model.stats_map = {}

    model.color = 'blue'
    assert model.color == 'blue'

    with pytest.raises(TypeError, match='read-only|not writable'):
        model.chips = {}

    assert isinstance(model.chip_dict, collections.abc.MutableMapping)
    with pytest.raises(TypeError, match='read-only|not writable'):
        model.chip_dict = {}

def test_stats_map():
    model = gtkmodel.AppModel()
    stats = model.stats
    stats_map = model.stats_map

    assert not list(stats)
    assert not stats_map

    stats_map['x'] = (10, 20)
    assert tuple(stats[0]) == ('x', True, 10, True, 20)
    assert stats_map['x'] == (10, 20)

    stats.append(('y', True, 0, True, 10))
    assert tuple(stats[1]) == ('y', True, 0, True, 10)
    assert stats_map['y'] == (0, 10)

    stats_map['z'] = (None, None)
    assert tuple(stats[2]) == ('z', False, 0, False, 0)
    assert stats_map['z'] == (None, None)

    stats.append(('w', False, -2**31, False, 2**31 - 1))
    assert tuple(stats[3]) == ('w', False, -2**31, False, 2**31 - 1)
    assert stats_map['w'] == (None, None)

    assert len(stats) == len(stats_map) == 4
    assert list(stats_map) == ['x', 'y', 'z', 'w']

    del stats_map['x']
    assert len(stats) == 3
    with pytest.raises(KeyError, match='x'):
        stats_map['x']
    with pytest.raises(KeyError, match='x'):
        del stats_map['x']

    stats_map['y'] = (30, 40)
    assert len(stats) == 3
    assert tuple(stats[0]) == ('y', True, 30, True, 40)
    assert stats_map['y'] == (30, 40)

def test_chip_dict():
    CHIP1 = Chip(Shape('xxxxxx'), a=10, b=20)
    CHIP2 = Chip(Shape('xxx/xxx'), y=30, z=30)

    model = gtkmodel.AppModel()
    chips = model.chips
    chip_dict = model.chip_dict

    assert not list(chips)
    assert not chip_dict

    chip_dict['orange'] = [CHIP1, CHIP2]
    assert list(chip_dict) == ['orange']
    assert chip_dict['orange'] == [CHIP1, CHIP2]

    assert len(chips) == len(chip_dict) == 1
    assert chips[0][0] == 'orange'
    assert [chip for chip, in chips[0][1]] == [CHIP1, CHIP2]

    chip_dict['orange'] = [CHIP1]
    assert len(chips) == len(chip_dict) == 1
    assert chips[0][0] == 'orange'
    assert [chip for chip, in chips[0][1]] == [CHIP1]

    del chip_dict['orange']
    with pytest.raises(KeyError, match='orange'):
        chip_dict['orange']
    with pytest.raises(KeyError, match='orange'):
        del chip_dict['orange']

def test_gtk_solver_model():
    param = Parameter(area=Shape('xxx'),
                      stats={'x': (None, None)},
                      chips=[Chip(Shape('xxx'), x=10)])

    model = gtkmodel.SolverModel(param=param)
    assert model.param == param
    with pytest.raises(TypeError):
        model.param = param

    with pytest.raises(TypeError, match='read-only|not writable'):
        model.solutions = Gtk.ListStore(Gtk.ListStore)

    solution = Solution([SolutionEntry(0, 0, 0, 0)])
    solution_row = model.solution_to_row(solution)
    assert len(solution_row[0]) == 1
    assert tuple(solution_row[0][0]) == (0, 0, 0, 0)
    assert solution_row[1] == 10

    assert model.row_to_solution(solution_row) == solution

    model.solutions.append(solution_row)

    path = Gtk.TreePath.new_from_string("0")
    focused_ref = Gtk.TreeRowReference.new(model.solutions, path)
    model.focused_ref = focused_ref
    assert tuple(model.focused_row) == tuple(solution_row)

    assert model.finished is False
    model.finished = True
    assert model.finished is True
