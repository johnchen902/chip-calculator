import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GObject
import pytest

import chipcalc.model as chipmodel
import chipcalc.gtk.model as gtkmodel
import chipcalc.gtk.widgets as gtkwidgets
from tests.gtk.helpers import realized, wait_until_idle

BGM71 = chipmodel.Shape('/'.join(['xxxxxx'] * 6))

def test_hoc_combo_box():
    app_model = gtkmodel.AppModel()

    with realized(gtkwidgets.HOCComboBox()) as box:
        box.app_model = app_model
        wait_until_idle()
        assert box.app_model == app_model
        assert box.get_active_iter() is None

        app_model.area = BGM71
        wait_until_idle()
        bgm71_active_iter = box.get_active_iter()
        assert bgm71_active_iter is not None
        # TODO test if "BGM-71" and "★★★★★" are displayed on screen

        app_model.area = chipmodel.Shape('xx/xx')
        wait_until_idle()
        assert box.get_active_iter() is None

        box.set_active_iter(bgm71_active_iter)
        wait_until_idle()
        assert app_model.area == BGM71

    with realized(gtkwidgets.HOCComboBox()) as box:
        box.app_model = app_model
        assert box.get_active_iter() is not None

def test_area_text_view():
    app_model = gtkmodel.AppModel()
    app_model.area = BGM71

    with realized(gtkwidgets.AreaTextView()) as textview:
        textview.app_model = app_model
        wait_until_idle()
        assert textview.app_model == app_model
        assert textview.get_buffer().get_property('text') == '\n'.join(['xxxxxx'] * 6)

        textview.get_buffer().insert(textview.get_buffer().get_end_iter(), '\nBAD AREA')
        wait_until_idle()
        assert app_model.area == BGM71  # unchanged

        textview.get_buffer().set_text('')
        wait_until_idle()
        assert app_model.area == chipmodel.Shape()

def test_stats_tree_view():
    app_model = gtkmodel.AppModel()

    with realized(gtkwidgets.StatsTreeView()) as treeview:
        treeview.app_model = app_model
        wait_until_idle()
        assert treeview.app_model == app_model

        app_model.stats_map['xxx'] = (10, 20)
        # TODO test treeview is displaying "xxx [✓] 10 [✓] 20"

        app_model.stats_map['yyy'] = (None, None)
        # treeview should display "yyy [ ] 0 [ ] 0"

        app_model.stats_map['xxx'] = (30, 40)
        # treeview should display "xxx [✓] 30 [✓] 40"

        # TODO test changing treeview changes app_model

def test_color_combo_box():
    app_model = gtkmodel.AppModel()

    with realized(GObject.new(gtkwidgets.ColorComboBox, has_entry=True)) as box:
        box.set_property('entry-text-column', 0)
        box.app_model = app_model
        wait_until_idle()
        assert box.app_model == app_model
        assert box.get_active_iter() is None

        app_model.color = 'blue'
        wait_until_idle()
        assert box.get_active_iter() is None
        assert box.get_child().get_text() == 'blue'

        app_model.chip_dict['orange'] = []
        app_model.color = 'orange'
        wait_until_idle()
        assert box.get_active_iter() is not None
        assert box.get_child().get_text() == 'orange'
        orange_iter = box.get_active_iter()

        box.get_child().set_text('blue')
        wait_until_idle()
        assert box.get_active_iter() is None
        assert app_model.color == 'blue'

        box.set_active_iter(orange_iter)
        wait_until_idle()
        assert box.get_child().get_text() == 'orange'
        assert app_model.color == 'orange'

def test_chips_tree_view():
    CHIP1 = chipmodel.Chip(chipmodel.Shape('xxxxxx'), d=9, db=2, x=10)

    app_model = gtkmodel.AppModel()

    with realized(gtkwidgets.ChipsTreeView()) as treeview:
        treeview.app_model = app_model
        wait_until_idle()
        assert treeview.app_model == app_model

        app_model.chip_dict['blue'] = [CHIP1]
        app_model.color = 'blue'
        wait_until_idle()
        assert len(treeview.get_model()) == 1
        assert tuple(treeview.get_model()[0]) == (CHIP1,)
        # TODO check the chip is shown on screen

        # TODO check user can edit the chip

def test_solution_count_header_bar():
    solver_model = gtkmodel.SolverModel()

    with realized(gtkwidgets.SolutionCountHeaderBar(solver_model=solver_model)) as bar:
        wait_until_idle()
        assert bar.get_title() == "No Solution"

        solver_model.solutions.append((None,))
        wait_until_idle()
        assert bar.get_title() == "1 Solution"

        solver_model.solutions.append((None,))
        wait_until_idle()
        assert bar.get_title() == "2 Solutions"

def _get_nontrivial_param():
    # Exactly 1 solution: [(0, 0, 0, 0)]

    area = chipmodel.Shape('xxx/xxx')
    chips = [
        chipmodel.Chip(chipmodel.Shape('xxx/xxx'), d=10),
        chipmodel.Chip(chipmodel.Shape('xxx/xxx'), p=10),
    ]
    stats = {
        'd': (10, None),
        '__len__': (6, None),
        '__rotate__': (None, 0),
    }

    return chipmodel.Parameter(area, chips, stats)

def test_solutions_tree_view():
    param = _get_nontrivial_param()
    solver_model = gtkmodel.SolverModel(param=param)
    solution = chipmodel.Solution([chipmodel.SolutionEntry(0, 0, 0, 0)])
    solution_row = solver_model.solution_to_row(solution)

    with realized(gtkwidgets.SolutionsTreeView(solver_model=solver_model)) as view:
        solver_model.solutions.append(solution_row)
        # TODO Test its appearance.

        path0 = Gtk.TreePath.new_from_string("0")
        view.get_selection().select_path(path0)
        assert tuple(solver_model.focused_row) == solution_row

def test_solution_area_view():
    param = _get_nontrivial_param()
    solver_model = gtkmodel.SolverModel(param=param)
    solution = chipmodel.Solution([chipmodel.SolutionEntry(0, 0, 0, 0)])
    solution_row = solver_model.solution_to_row(solution)

    with realized(gtkwidgets.SolutionAreaView(solver_model=solver_model)) as view:
        solver_model.solutions.append(solution_row)

        path0 = Gtk.TreePath.new_from_string("0")
        ref = Gtk.TreeRowReference.new(solver_model.solutions, path0)
        solver_model.focused_ref = ref
        # TODO Test its appearance.

def test_solution_view():
    param = _get_nontrivial_param()
    solver_model = gtkmodel.SolverModel(param=param)
    solution = chipmodel.Solution([chipmodel.SolutionEntry(0, 0, 0, 0)])
    solution_row = solver_model.solution_to_row(solution)

    with realized(gtkwidgets.SolutionView(solver_model=solver_model)) as view:
        solver_model.solutions.append(solution_row)

        path0 = Gtk.TreePath.new_from_string("0")
        ref = Gtk.TreeRowReference.new(solver_model.solutions, path0)
        solver_model.focused_ref = ref
        # TODO Test its appearance.
