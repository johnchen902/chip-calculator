import sys

import pytest

@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_call(item):
    __tracebackhide__ = True

    old_hook = sys.excepthook

    exception = None
    def _on_hook(tp, value, traceback):
        nonlocal exception
        if exception is None:
            exception = tp, value, traceback
        else:
            old_hook(tp, value, traceback)

    sys.excepthook = _on_hook
    try:
        yield
    finally:
        sys.excepthook = old_hook

    if exception is not None:
        tp, value, traceback = exception
        raise tp(value).with_traceback(traceback)
