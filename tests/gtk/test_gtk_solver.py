import time

import pytest

from tests.gtk.helpers import wait_until_idle
import chipcalc.gtk.model as gtkmodel
import chipcalc.gtk.solver as gtksolver
import chipcalc.model as chipmodel
import chipcalc.solver as chipsolver

def wait_for_solver(model):
    while not model.finished:
        time.sleep(0.1)
        wait_until_idle()

    assert model.finished is True

def test_gtk_solver():
    area = chipmodel.Shape('xxx/xxx')
    chips = [
        chipmodel.Chip(chipmodel.Shape('xxx/xxx'), d=10),
        chipmodel.Chip(chipmodel.Shape('xxx/xxx'), p=10),
    ]
    stats = {
        'd': (10, None),
        '__len__': (6, None),
        '__rotate__': (None, 0),
    }

    param = chipmodel.Parameter(area, chips, stats)
    model = gtkmodel.SolverModel(param=param)
    solver = chipsolver.Solver(param)

    gtksolver.feed_to_solver_model(solver, model)
    wait_for_solver(model)

    assert len(model.solutions) == 1

    solution = model.row_to_solution(model.solutions[0])
    assert len(solution) == 1
    assert tuple(solution[0]) == (0, 0, 0, 0)

def test_gtk_solver_rotate():
    area = chipmodel.Shape('xx/xx/xx')
    chips = [
        chipmodel.Chip(chipmodel.Shape('xxx/xxx'), d=10),
        chipmodel.Chip(chipmodel.Shape('xxx/xxx'), p=10),
    ]
    stats = {
        'd': (10, None),
        '__len__': (6, None),
    }

    param = chipmodel.Parameter(area, chips, stats)
    model = gtkmodel.SolverModel(param=param)
    solver = chipsolver.Solver(param)

    gtksolver.feed_to_solver_model(solver, model)
    wait_for_solver(model)

    assert len(model.solutions) == 1

    solution = model.row_to_solution(model.solutions[0])
    assert len(solution) == 1
    assert tuple(solution[0]) == (0, 0, 0, 1)

def test_gtk_solver_norotate():
    area = chipmodel.Shape('xx/xx/xx')
    chips = [
        chipmodel.Chip(chipmodel.Shape('xxx/xxx'), d=10),
        chipmodel.Chip(chipmodel.Shape('xxx/xxx'), p=10),
    ]
    stats = {
        'd': (10, None),
        '__len__': (6, None),
        '__rotate__': (None, 0),
    }

    param = chipmodel.Parameter(area, chips, stats)
    model = gtkmodel.SolverModel(param=param)
    solver = chipsolver.Solver(param)

    gtksolver.feed_to_solver_model(solver, model)
    wait_for_solver(model)

    assert len(model.solutions) == 0

def test_gtk_solver_replace():
    sol1 = chipmodel.Solution([chipmodel.SolutionEntry(0, 0, 0, 2)])
    sol2 = chipmodel.Solution([chipmodel.SolutionEntry(0, 0, 0, 1)])
    sol3 = chipmodel.Solution([chipmodel.SolutionEntry(0, 0, 0, 0)])

    class MockSolver:
        def run(*args, **kwargs):
            yield chipsolver.Command.NEW, sol1
            yield chipsolver.Command.REPLACE, sol1, sol2
            yield chipsolver.Command.REPLACE, sol2, sol3

    model = gtkmodel.SolverModel()

    gtksolver.feed_to_solver_model(MockSolver(), model)
    wait_for_solver(model)

    assert len(model.solutions) == 1

    solution, = model.solutions[0]
    assert len(solution) == 1
    assert tuple(solution[0]) == (0, 0, 0, 0)

@pytest.mark.timeout(10)
def test_solver_cancel():
    area = chipmodel.Shape('x' * 10)
    chips = [chipmodel.Chip(chipmodel.Shape('x'))] * 10

    param = chipmodel.Parameter(area, chips, {})
    model = gtkmodel.SolverModel(param=param)
    solver = chipsolver.Solver(param)

    gtksolver.feed_to_solver_model(solver, model)
    model.emit('stop')
    wait_for_solver(model)
