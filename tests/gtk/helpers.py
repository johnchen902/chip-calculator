import contextlib

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

def wait_until_idle():
    while Gtk.events_pending():
        Gtk.main_iteration()

@contextlib.contextmanager
def realized(widget):
    if widget.is_toplevel():
        window = None
    else:
        window = Gtk.Window()
        window.add(widget)

    widget.realize()
    wait_until_idle()
    assert widget.get_realized()
    yield widget

    if window is not None:
        window.remove(widget)
        window.destroy()

    wait_until_idle()
