import pytest

from chipcalc.model import Shape, Chip, Parameter, Solution
import chipcalc.solver as chipsolver


@pytest.fixture(params=[chipsolver.LocalSolver, chipsolver.Solver])
def solver_factory(request):
    return request.param

def find_all_solutions(param, solver_factory):
    return chipsolver.find_all_solutions(param, solver_factory=solver_factory)

def test_solver(solver_factory):
    area = Shape('xxx/xxx')
    chips = [
        Chip(Shape('xxx/xxx'), d=10),
        Chip(Shape('xxx/xxx'), p=10),
    ]
    stats = {
        'd': (10, None),
        '__len__': (6, None),
        '__rotate__': (None, 0),
    }

    param = Parameter(area, chips, stats)

    sol = Solution([(0, 0, 0, 0)])

    assert find_all_solutions(param, solver_factory) == [sol]

def test_solver_rotate(solver_factory):
    area = Shape('xx/xx/xx')
    chips = [
        Chip(Shape('xxx/xxx'), d=10),
        Chip(Shape('xxx/xxx'), p=10),
    ]
    stats = {
        'd': (10, None),
        '__len__': (6, None),
    }

    param = Parameter(area, chips, stats)

    sol = Solution([(0, 0, 0, 1)])

    assert find_all_solutions(param, solver_factory) == [sol]

def test_solver_norotate(solver_factory):
    area = Shape('xx/xx/xx')
    chips = [
        Chip(Shape('xxx/xxx'), d=10),
        Chip(Shape('xxx/xxx'), p=10),
    ]
    stats = {
        'd': (10, None),
        '__len__': (6, None),
        '__rotate__': (None, 0),
    }

    param = Parameter(area, chips, stats)

    assert not find_all_solutions(param, solver_factory)

def test_solver_double_run(solver_factory):
    param = Parameter(Shape('x' * 10), [Chip(Shape('x'))], {})

    solver = solver_factory(param)
    chipsolver.process_solver_run(solver.run())

    with pytest.raises(ValueError):
        chipsolver.process_solver_run(solver.run())

@pytest.mark.timeout(10)
def test_solver_cancel(solver_factory):
    param = Parameter(Shape('x' * 10), [Chip(Shape('x'))] * 10, {})

    solver = solver_factory(param)

    gen = solver.run()
    next(gen)
    solver.cancel()

    chipsolver.process_solver_run(gen)

def test_solver_cancel_before_run(solver_factory):
    param = Parameter(Shape('x' * 10), [Chip(Shape('x'))] * 10, {})

    solver = solver_factory(param)
    solver.cancel()

    assert not chipsolver.process_solver_run(solver.run())

def test_solver_cancel_after_run(solver_factory):
    param = Parameter(Shape('x' * 10), [Chip(Shape('x'))], {})

    solver = solver_factory(param)

    chipsolver.process_solver_run(solver.run())
    solver.cancel()
