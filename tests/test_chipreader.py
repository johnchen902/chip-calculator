import pytest
from chipcalc.model import Shape, Chip
import chipcalc.chipreader as cr

CHIP1 = Chip(Shape('xxx/xxx'), d=14, p=39)
CHIP2 = Chip(Shape('..xx/xxx'), d=15, r=5)

CHIPS1 = [CHIP1, CHIP2]
STR_CHIPS1 = 'xxx/xxx d=14 p=39\n..xx/xxx d=15 r=5\n'

CHIP_DICT1 = {'blue': [], 'orange': CHIPS1}
STR_CHIP_DICT1 = '[blue-chips]\n[orange-chips]\n' + STR_CHIPS1

def test_format_chips():
    assert cr.format_chips([]) == ''
    assert cr.format_chips(CHIPS1) == STR_CHIPS1

    with pytest.raises(TypeError):
        cr.format_chips(CHIP_DICT1)

def test_format_chip_dict():
    assert cr.format_chip_dict({}) == ''
    assert cr.format_chip_dict(CHIP_DICT1) == STR_CHIP_DICT1

    with pytest.raises(TypeError):
        cr.format_chip_dict(CHIPS1)

def test_parse_chips():
    assert cr.parse_chips('') == []
    assert cr.parse_chips(STR_CHIPS1) == CHIPS1

    with pytest.raises(ValueError, match='unexpected section'):
        cr.parse_chips(STR_CHIP_DICT1)

    with pytest.raises(ValueError, match='invalid chip'):
        cr.parse_chips('yyy\n')

def test_parse_chip_dict():
    assert cr.parse_chip_dict('') == {}
    assert cr.parse_chip_dict(STR_CHIP_DICT1) == CHIP_DICT1

    with pytest.raises(ValueError, match='no color'):
        cr.parse_chip_dict(STR_CHIPS1)

    with pytest.raises(ValueError, match='section name'):
        cr.parse_chip_dict('[blue]\n')

    with pytest.raises(ValueError, match='duplicate color'):
        cr.parse_chip_dict('[blue-chips]\n[blue-chips]')

    with pytest.raises(ValueError, match='invalid chip'):
        cr.parse_chip_dict('[blue-chips]\nyyy\n')
