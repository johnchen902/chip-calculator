import glob
import os.path

import cv2 as cv
import numpy as np
import pytest

from chipcalc.model import Shape, Chip
import chipcalc.cv

@pytest.fixture
def chip_finder():
    return chipcalc.cv.ChipFinder()

def load_chips(path):
    chips = []
    for line in open(path):
        if not line:
            continue
        chips.append(Chip.parse(line))
    return chips

@pytest.fixture(params=range(1, 3))
def chip_image(request):
    x = request.param

    imgpath = os.path.join('tests', 'cv_data', f'img-{x}.*')
    imgpath = glob.glob(imgpath)[0]
    img = chipcalc.cv.imread(imgpath)

    chipspath = os.path.join('tests', 'cv_data', f'chips-{x}.txt')
    chips = load_chips(chipspath)

    return img, chips

@pytest.mark.parametrize('image', [
    np.zeros((500, 500, 3), dtype=np.uint8)
])
def test_cv_empty(chip_finder, image):
    assert chip_finder.find(image) == []

def test_cv(chip_finder, chip_image):
    img, chips = chip_image
    assert chip_finder.find(img) == chips
