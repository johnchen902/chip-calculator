import os.path

from chipcalc.model import Shape, Chip
import chipcalc.hycdes as hycdes
import chipcalc.gfl as gfl

def make_chip(shape, level):
    chip = Chip(Shape(shape), db=1, pb=1, ab=1, rb=len(shape) - 3, stars=5)
    return gfl.with_level(chip, level)

def test_hycdes():
    chips = [make_chip(shape, level % 21)
             for level, shape in enumerate(hycdes.SHAPE_CODES.values())]
    chip_dict = {'blue': chips, 'orange': []}

    savecode = hycdes.from_chip_dict(chip_dict)

    # Can we ensure savecode is really a valid one?

    assert hycdes.to_chip_dict(savecode) == chip_dict
