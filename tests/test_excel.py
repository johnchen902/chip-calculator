import os.path

from chipcalc.model import Shape, Chip
import chipcalc.excel as ce
import chipcalc.gfl as gfl

def make_chip(shape, level):
    chip = Chip(Shape(shape), db=1, pb=1, ab=1, rb=len(shape) - 3, stars=5)
    return gfl.with_level(chip, level)

def test_excel(tmpdir):
    chips = [make_chip(shape, level % 21)
             for level, shape in enumerate(ce.SHAPE_CODES.values())]
    path = os.path.join(tmpdir, 'test.xlsx')

    ce.export_chips(path, chips)

    sheetname = ce.get_sheetnames(path)[0]

    imported_chips = ce.import_chips(path, sheetname, 'A2:A501', 'B2:E501')

    for chip, imported_chip in zip(chips, imported_chips):
        # Level and stars are discarded.

        assert chip in gfl.get_possible_full_chips(imported_chip)
