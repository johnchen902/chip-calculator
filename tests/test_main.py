import sys

import pytest

from chipcalc.model import Shape, Chip, Solution
import chipcalc.__main__ as chipmain

class FakeArgs:
    pass

def test_get_stats():
    prog_args = FakeArgs()

    prog_args.constraint = []
    assert chipmain.get_stats(prog_args) == {}

    prog_args.constraint = ['x,,']
    assert chipmain.get_stats(prog_args) == {'x': (None, None)}

    prog_args.constraint = ['x,10,', 'y,,0']
    assert chipmain.get_stats(prog_args) == {'x': (10, None), 'y': (None, 0)}

def test_get_chips(tmp_path):
    prog_args = FakeArgs()

    p = tmp_path / 'chips.txt'
    p.write_text('xxx/xxx a=1 b=2\n')
    prog_args.color = None
    prog_args.chips = p.open()
    assert chipmain.get_chips(prog_args) == [Chip(Shape('xxx/xxx'), a=1, b=2)]

    p = tmp_path / 'chip_dict.txt'
    p.write_text('[blue-chips]\nxx/xx/xx a=1 b=2\n')
    prog_args.color = 'blue'
    prog_args.chips = p.open()
    assert chipmain.get_chips(prog_args) == [Chip(Shape('xx/xx/xx'), a=1, b=2)]

def test_get_formatted_area():
    area = Shape('xxxxx/.xxxxx')

    chips = [Chip(Shape('xxxx')), Chip(Shape('/..xxxx'))]

    assert chipmain.get_formatted_area(area, chips) == 'aaaa_\n._bbbb'

@pytest.mark.parametrize('local', [False, True])
@pytest.mark.parametrize('use_color', [False, True])
def test_main(monkeypatch, capsys, tmp_path, local, use_color):
    p = tmp_path / 'chips.txt'
    argv = [sys.argv[0],
            '--area', 'xxxxxx/xxxxxx',
            '--chips', str(p),
            '--constraint', '__len__,12,',
            '--constraint', '__rotate__,,0']

    if local:
        argv.append('--local')

    if use_color:
        argv.append('--color')
        argv.append('blue')
        p.write_text('''
            [blue-chips]
            xxxxx/x a=1 b=2
            ....x/xxxxx a=3 b=4
            x/x/x/x/xx a=5 b=6
        ''')
    else:
        p.write_text('''
            xxxxx/x a=1 b=2
            ....x/xxxxx a=3 b=4
            x/x/x/x/xx a=5 b=6
        ''')

    monkeypatch.setattr(sys, 'argv', argv)

    assert not chipmain.main()

    captured = capsys.readouterr()

    assert captured.out == ('sol_id\t__len__\t__rotate__\n'
                            '1\t12\t0\t[0, 1]\taaaaab/abbbbb\n')
    assert not captured.err
