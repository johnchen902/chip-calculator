numpy==1.17.0
opencv-python-headless==4.1.0.25
openpyxl==2.6.3
ortools==7.3.7083
pycairo==1.18.1
PyGObject==3.32.2
