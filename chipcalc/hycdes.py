"""Imports and exports hycdes.com savecode."""
from chipcalc.model import Shape, Chip
import chipcalc.gfl as gfl

SHAPE_CODES = {
    ('56', '1'): Shape('xx/xx/xx'),
    ('56', '2'): Shape('xxx/xx/x'),
    ('56', '3'): Shape('xxxx/.xx'),
    ('56', '41'): Shape('xxx/.xxx'),
    ('56', '42'): Shape('.xxx/xxx'),
    ('56', '5'): Shape('xx/.xx/xx'),
    ('56', '6'): Shape('.x/xxx/.x/.x'),
    ('56', '7'): Shape('x/x/x/x/x/x'),
    ('56', '8'): Shape('xxxx/x..x'),
    ('56', '9'): Shape('xx/xxx/.x'),
    ('551', '11'): Shape('.xx/xx/.x'),
    ('551', '12'): Shape('xx/.xx/.x'),
    ('551', '21'): Shape('xxx/..xx'),
    ('551', '22'): Shape('.xxx/xx'),
    ('551', '31'): Shape('.x/xx/.x/.x'),
    ('551', '32'): Shape('x/xx/x/x'),
    ('551', '4'): Shape('xxx/.x/.x'),
    ('551', '5'): Shape('.xx/xx/x'),
    ('551', '6'): Shape('.x/xxx/.x'),
    ('551', '81'): Shape('x/xx/xx'),
    ('551', '82'): Shape('.x/xx/xx'),
    ('551', '9'): Shape('x/x/x/x/x'),
    ('551', '10'): Shape('xx/x/xx'),
    ('551', '111'): Shape('xx/.x/.xx'),
    ('551', '112'): Shape('.xx/.x/xx'),
    ('551', '120'): Shape('xxx/x/x'),
    ('551', '131'): Shape('x/x/x/xx'),
    ('551', '132'): Shape('.x/.x/.x/xx'),
}

COLOR_CODES = {
    '1': 'blue',
    '2': 'orange',
}

REVERSE_SHAPE_CODES = {mask: code for code, ref_mask in SHAPE_CODES.items()
                       for mask in ref_mask.rotations()}

REVERSE_COLOR_CODES = {color: code for code, color in COLOR_CODES.items()}

MAGIC_START = 'InfinityFrost'
MAGIC_END = 'FatalChapters'

def to_chip_dict(hycdes):
    """Convert hycdes.com savecode to {color: [Chip]}"""
    hycdes = hycdes[hycdes.index('!') + 1 : hycdes.index('?')]
    hycdes = hycdes.rstrip('&').split('&')

    chip_dict = {color: [] for color in REVERSE_COLOR_CODES}
    for hline in hycdes:
        _, color, cls, shape, level, acc, rel, dmg, pen, _ = hline.split(',')

        try:
            mask = SHAPE_CODES[cls, shape]
        except KeyError as exc:
            raise ValueError(f'Unknown classNum {cls} or typeNum {shape}') from exc

        try:
            color = COLOR_CODES[color]
        except KeyError as exc:
            raise ValueError(f'Unknown color {color}') from exc

        level = int(level)
        acc = int(acc)
        rel = int(rel)
        dmg = int(dmg)
        pen = int(pen)

        chip = Chip(mask, db=dmg, pb=pen, ab=acc, rb=rel, stars=5)

        chip_dict[color].append(gfl.with_level(chip, level))

    return chip_dict


def _check_chip(chip):
    if 'stars' not in chip.stats:
        raise ValueError(f"missing 'stars': {chip}")

    if chip['stars'] != 5:
        raise ValueError(f"'stars' must be 5: {chip}")

    if not any(x in chip.stats for x in ['ab', 'rb', 'db', 'pb']):
        raise ValueError(f"missing 'ab', 'rb', 'db, or 'pb': {chip}")

    total_blocks = chip['ab'] + chip['rb'] + chip['db'] + chip['pb']
    if total_blocks != len(chip.shape):
        raise ValueError(f'wrong number of blocks: {chip}')


def from_chip_dict(chip_dict):
    """Convert {color: [Chip]} to hycdes.com savecode"""
    chips = [(color, chip) for color, cl in chip_dict.items()
             if color in REVERSE_COLOR_CODES
             for chip in cl]

    lines = []
    for i, (color, chip) in enumerate(chips, start=1):
        color = REVERSE_COLOR_CODES[color]

        try:
            cls, shape = REVERSE_SHAPE_CODES[chip.shape]
        except KeyError as exc:
            raise ValueError(f'unsupported shape: {chip}') from exc

        _check_chip(chip)

        lines.append(f"{i},{color},{cls},{shape},{chip['level']},"
                     f"{chip['ab']},{chip['rb']},{chip['db']},{chip['pb']},1&")

    magic1 = MAGIC_START[len(chips) % len(MAGIC_START)]
    body = ''.join(lines)
    magic2 = MAGIC_END[len(chips) % len(MAGIC_END)]

    return f'[{magic1}!{body}?{magic2}]'
