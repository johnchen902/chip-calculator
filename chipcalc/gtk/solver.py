"""Solver related code without better place."""
import threading

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib

import chipcalc.solver as chipsolver
import chipcalc.gtk.actions as gtkactions
import chipcalc.gtk.model as gtkmodel

class _CommandHandler:
    def __init__(self, solver_model):
        self._solver_model = solver_model
        self._it_map = {}

    def _new_solution(self, solution):
        solution_row = self._solver_model.solution_to_row(solution)
        it = self._solver_model.solutions.append(solution_row)
        self._it_map[tuple(solution)] = it

    def _replace_solution(self, old_solution, new_solution):
        try:
            it = self._it_map.pop(tuple(old_solution))
        except KeyError as exc:
            gtkactions.show_error(exc)
            self._new_solution(new_solution)
            return

        solution_row = self._solver_model.solution_to_row(new_solution)
        self._solver_model.solutions.set_row(it, solution_row)
        self._it_map[tuple(new_solution)] = it

    def _command_received(self, command):
        cmd_type = command[0]
        if cmd_type == chipsolver.Command.NEW:
            _, solution = command
            self._new_solution(solution)
        elif cmd_type == chipsolver.Command.REPLACE:
            _, old_solution, new_solution = command
            self._replace_solution(old_solution, new_solution)
        elif cmd_type == chipsolver.Command.ERROR:
            gtkactions.show_error(command[1])
        else:
            error = AssertionError(f'unexpected command: {cmd_type}')
            gtkactions.show_error(error)

    def _solver_ended(self):
        self._solver_model.finished = True

def feed_to_solver_model(solver, solver_model):
    """Feed the solver's output to model.

    Args:
        solver: a chipcalc.solver.Solver-like object
        model: a chipcalc.gtk.model.SolverModel
    """

    solver_model.connect('stop', lambda _: solver.cancel())

    def _thread_target():
        handler = _CommandHandler(solver_model)
        for command in solver.run():
            GLib.idle_add(handler._command_received, command)
        GLib.idle_add(handler._solver_ended)

    thread = threading.Thread(target=_thread_target, daemon=True)
    thread.start()
