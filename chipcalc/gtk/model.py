"""Define some GTK-style models"""
import collections.abc

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GObject, Gtk

import chipcalc.model as chipmodel

class StatsMap(collections.abc.MutableMapping):
    """Wraps Gtk.ListModel(str, int, int) as a dict-like object."""

    def __init__(self, stats):
        collections.abc.MutableMapping.__init__(self)
        self._stats = stats

    def __len__(self):
        return len(self._stats)

    def __iter__(self):
        for stat, *_ in self._stats:
            yield stat

    def __getitem__(self, key):
        for stat, has_lb, lower_bound, has_ub, upper_bound in self._stats:
            if stat == key:
                if not has_lb:
                    lower_bound = None
                if not has_ub:
                    upper_bound = None
                return lower_bound, upper_bound
        raise KeyError(key)

    def get_iter(self, key):
        """Get Gtk.TreeIter associated with this key."""

        it = self._stats.get_iter_first()
        while it is not None:
            if self._stats.get_value(it, 0) == key:
                return it
            it = self._stats.iter_next(it)
        return None

    def __setitem__(self, key, value):
        lower_bound, upper_bound = value
        has_lb = lower_bound is not None
        has_ub = upper_bound is not None

        it = self.get_iter(key)
        if it is not None:
            self._stats.set(it, (1, 2, 3, 4), (has_lb, lower_bound,
                                               has_ub, upper_bound))
        else:
            self._stats.append((key, has_lb, lower_bound,
                                has_ub, upper_bound))

    def __delitem__(self, key):
        it = self.get_iter(key)
        if it is None:
            raise KeyError(key)
        self._stats.remove(it)

    # TODO optimize items, values, etc.

class ChipDict(collections.abc.MutableMapping):
    """Wraps Gtk.ListModel(str, Gtk.ListModel) as a dict-like object."""

    def __init__(self, chips):
        collections.abc.MutableMapping.__init__(self)
        self._chips = chips

    def __len__(self):
        return len(self._chips)

    def __iter__(self):
        for color, _ in self._chips:
            yield color

    def __getitem__(self, key):
        for color, chips in self._chips:
            if color == key:
                return [chip for chip, in chips]
        raise KeyError(key)

    def get_iter(self, key):
        """Get Gtk.TreeIter associated with this key."""

        it = self._chips.get_iter_first()
        while it is not None:
            if self._chips.get_value(it, 0) == key:
                return it
            it = self._chips.iter_next(it)
        return None

    def __setitem__(self, key, value):
        it = self.get_iter(key)
        if it is not None:
            list_store = self._chips.get_value(it, 1)
            list_store.clear()
        else:
            list_store = Gtk.ListStore(object)
            self._chips.append((key, list_store))

        for chip in value:
            list_store.append((chip,))

    def __delitem__(self, key):
        it = self.get_iter(key)
        if it is None:
            raise KeyError(key)
        self._chips.remove(it)

class AppModel(GObject.Object):
    """Model of the app."""

    def __init__(self, **kwargs):
        self._area = chipmodel.Shape()
        self._stats = Gtk.ListStore(str, bool, int, bool, int)
        self._stats_map = StatsMap(self._stats)
        self._color = ''
        self._chips = Gtk.ListStore(str, Gtk.ListStore)
        self._chip_dict = ChipDict(self._chips)

        GObject.Object.__init__(self, **kwargs)

        # TODO maybe we'll need this
        # self._save_path = None
        # self._edited = False
        # self._chip_text = ''

    @GObject.Property(flags=GObject.ParamFlags.READWRITE |
                      GObject.ParamFlags.EXPLICIT_NOTIFY)
    def area(self):
        """Area of the HOC."""
        return self._area

    @area.setter
    def set_area(self, area):
        if area != self._area:
            self._area = area
            self.notify('area')

    @GObject.Property(flags=GObject.ParamFlags.READABLE,
                      type=Gtk.ListStore)
    def stats(self):
        """Constraints of intended solutions as Gtk.ListStore.

        column 0 (str): stats
        column 1 (bool): has minimum
        column 2 (int): minimum
        column 3 (bool): has maximum
        column 4 (int): maximum
        """
        return self._stats

    @GObject.Property(flags=GObject.ParamFlags.READABLE)
    def stats_map(self):
        """Constraints of intended solutions as a dict-like object."""
        return self._stats_map

    @GObject.Property(flags=GObject.ParamFlags.READWRITE |
                      GObject.ParamFlags.EXPLICIT_NOTIFY)
    def color(self):
        """Color of chips to use."""
        return self._color

    @color.setter
    def set_color(self, color):
        if color != self._color:
            self._color = color
            self.notify('color')

    @GObject.Property(flags=GObject.ParamFlags.READABLE,
                      type=Gtk.ListStore)
    def chips(self):
        """Available chips as Gtk.ListStore(str, Gtk.ListStore)."""
        return self._chips

    @GObject.Property(flags=GObject.ParamFlags.READABLE)
    def chip_dict(self):
        """Available chips as a dict-like object."""
        return self._chip_dict

class SolverModel(GObject.Object):
    """Model for solutions."""

    __gsignals__ = {
        'stop': (GObject.SignalFlags.ACTION, None, tuple()),
    }

    def __init__(self, **kwargs):
        self._param = None
        self._solutions = None
        self._focused_ref = None
        self._finished = False

        GObject.Object.__init__(self, **kwargs)

        if self._param is None:
            self._solutions = Gtk.ListStore(Gtk.ListStore)
        else:
            n_stats = len(self._param.stats)
            self._solutions = Gtk.ListStore(Gtk.ListStore, *[int] * n_stats)

    @GObject.Property(flags=GObject.ParamFlags.READWRITE |
                      GObject.ParamFlags.CONSTRUCT_ONLY)
    def param(self):
        """Parameters for this computation."""
        return self._param

    @param.setter
    def set_param(self, param):
        self._param = param

    @GObject.Property(flags=GObject.ParamFlags.READABLE,
                      type=Gtk.ListStore)
    def solutions(self):
        """Gtk.ListStore of solutions.

        Each solution is a Gtk.ListStore with:
        column 0: chip id
        column 1: shift x
        column 2: shift y
        column 3: rotation
        """
        return self._solutions

    def solution_to_row(self, solution):
        """Returns a suitable row to be inserted into solutions."""
        chips_store = Gtk.ListStore(int, int, int, int)
        for row in solution:
            chips_store.append(row)

        if self._param is None:
            return chips_store,
        else:
            chips = self._param.chips
            stats = self._param.stats
            return (chips_store,
                    *(solution.get_stat(stat=stat, chips=chips)
                      for stat in stats))

    def row_to_solution(self, row):
        """Returns solution from a suitable row."""
        chips_store = row[0]
        return chipmodel.Solution(chipmodel.SolutionEntry(*row)
                                  for row in chips_store)

    @GObject.Property(flags=GObject.ParamFlags.READWRITE |
                      GObject.ParamFlags.EXPLICIT_NOTIFY,
                      type=Gtk.TreeRowReference)
    def focused_ref(self):
        """The focused solution."""
        return self._focused_ref

    @focused_ref.setter
    def set_focused_ref(self, focused_ref):
        if focused_ref != self._focused_ref:
            self._focused_ref = focused_ref
            self.notify('focused_ref')

    @GObject.Property(flags=GObject.ParamFlags.READABLE)
    def focused_row(self):
        if self._focused_ref is None:
            return None

        ref = self._focused_ref
        if not ref.valid():
            return None

        return ref.get_model()[ref.get_path()]

    @GObject.Property(flags=GObject.ParamFlags.READWRITE |
                      GObject.ParamFlags.EXPLICIT_NOTIFY,
                      type=bool, default=False)
    def finished(self):
        """Whether the computation has finished."""
        return self._finished

    @finished.setter
    def set_finished(self, finished):
        """Whether the computation has finished."""
        if finished != self._finished:
            self._finished = finished
            self.notify('finished')
