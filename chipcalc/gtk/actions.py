import dataclasses
import pkgutil

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GObject, Gdk, Gio, Gtk

import chipcalc.chipreader as chipreader
import chipcalc.gfl as gfl
import chipcalc.gtk.model as gtkmodel
import chipcalc.gtk.solver as gtksolver
import chipcalc.hycdes
import chipcalc.model as chipmodel
import chipcalc.solver as chipsolver

def show_error(exc):
    """Shows the exception."""

    dialog = Gtk.MessageDialog(
        flags=Gtk.DialogFlags.DESTROY_WITH_PARENT,
        text=exc.__class__.__name__,
        secondary_text=str(exc),
        message_type=Gtk.MessageType.ERROR,
        buttons=Gtk.ButtonsType.CLOSE)
    dialog.run()
    dialog.destroy()

def solve(action, param, app_model):
    del action, param

    try:
        chips = app_model.chip_dict[app_model.color]
    except KeyError as exc:
        show_error(exc)
        return

    param = chipmodel.Parameter(area=app_model.area,
                                stats=dict(app_model.stats_map),
                                chips=chips)

    solver = chipsolver.Solver(param)
    solver_model = gtkmodel.SolverModel(param=param)
    gtksolver.feed_to_solver_model(solver, solver_model)

    builder = Gtk.Builder.new()
    builder.expose_object('solver_model', solver_model)
    builder_xml = pkgutil.get_data('chipcalc.gtk', 'solver.ui')
    builder.add_from_string(builder_xml.decode('utf8'))

    solver_window = builder.get_object('solver-window')
    solver_window.connect('destroy', lambda _: solver_model.emit('stop'))
    solver_window.show_all()

class _MergeChipsContext:

    RESPONSE_MERGE = 0
    RESPONSE_IGNORE = 1
    RESPONSE_REPLACE = 2

    def __init__(self):
        # TODO "apply to all" option
        # self._default_action = None

        self._cancelled = False

    def merge_chip_dict(self, app_model, chip_dict):
        """Merges chip_dict into app_model."""
        self._cancelled = False

        # compute new chips but don't apply yet
        new_chip_dict = {}
        for color, chips in chip_dict.items():
            old_chips = app_model.chip_dict.get(color, [])
            new_chips = self.merge_chips(color, old_chips, chips)

            if self._cancelled:
                return

            new_chip_dict[color] = new_chips

        # apply the new chips
        for color, chips in new_chip_dict.items():
            app_model.chip_dict[color] = chips

    def merge_chips(self, color, old_chips, extra_chips):
        """Merges old_chips and extra_chips."""
        if not old_chips:
            return extra_chips
        if not extra_chips:
            return old_chips

        dialog = Gtk.MessageDialog(
            flags=Gtk.DialogFlags.DESTROY_WITH_PARENT,
            text=f"Color {color!r} is in both current chips and imported chips",
            secondary_text="Merge: merge current chips and imported chips\n"
            "Ignore: ignore imported chips\n"
            "Replace: replace current chips with imported chips",
            message_type=Gtk.MessageType.QUESTION)
        dialog.add_buttons("Merge", self.RESPONSE_MERGE,
                           "Ignore", self.RESPONSE_IGNORE,
                           "Replace", self.RESPONSE_REPLACE)
        result = dialog.run()
        dialog.destroy()

        if result == self.RESPONSE_MERGE:
            return old_chips + extra_chips
        if result == self.RESPONSE_IGNORE:
            return old_chips
        if result == self.RESPONSE_REPLACE:
            return extra_chips

        if result == Gtk.ResponseType.DELETE_EVENT:
            self._cancelled = True
            return None

        show_error(RuntimeError(f'Unhandled response {result}'))

def merge_chip_dict(app_model, chip_dict):
    """Merges chip_dict into app_model."""

    _MergeChipsContext().merge_chip_dict(app_model, chip_dict)


def import_text(action, param, app_model):
    """Imports chips from text file."""
    del action, param

    file_filter = Gtk.FileFilter()
    file_filter.add_mime_type('text/plain')

    chooser = Gtk.FileChooserNative(
        title='Open File',
        action=Gtk.FileChooserAction.OPEN,
        accept_label='_Open',
        cancel_label='_Cancel',
        filter=file_filter)

    result = chooser.run()
    if result != Gtk.ResponseType.ACCEPT:
        return

    path = chooser.get_filename()

    try:
        chip_dict = chipreader.read_chip_dict(open(path, encoding='utf-8-sig'))
    except (OSError, ValueError) as exc:
        show_error(exc)
        return

    merge_chip_dict(app_model, chip_dict)


def export_text(action, param, app_model):
    """Exports chips to text file."""
    del action, param

    file_filter = Gtk.FileFilter()
    file_filter.add_mime_type('text/plain')

    chooser = Gtk.FileChooserNative(
        title='Save File',
        action=Gtk.FileChooserAction.SAVE,
        accept_label='_Save',
        cancel_label='_Cancel',
        filter=file_filter)

    result = chooser.run()
    if result != Gtk.ResponseType.ACCEPT:
        return

    path = chooser.get_filename()

    try:
        with open(path, 'w', encoding='utf-8') as fobj:
            chipreader.write_chip_dict(app_model.chip_dict, fobj)
    except (OSError, ValueError) as exc:
        show_error(exc)


def import_hycdes(action, param, app_model):
    """Imports chips from hycdes.com savecode."""
    del action, param

    dialog = Gtk.MessageDialog(
        text='Enter hycdes.com savecode',
        secondary_text='Note that the shape is only correct up to rotation.',
        message_type=Gtk.MessageType.QUESTION,
        buttons=Gtk.ButtonsType.OK_CANCEL,
        modal=True,
        destroy_with_parent=True)

    entry = Gtk.Entry()

    dialog.get_content_area().add(entry)

    try:
        dialog.show_all()
        response = dialog.run()
        savecode = entry.get_text()
    finally:
        dialog.destroy()

    if response != Gtk.ResponseType.OK:
        return

    try:
        chip_dict = chipcalc.hycdes.to_chip_dict(savecode)
    except ValueError as exc:
        show_error(exc)
        return

    merge_chip_dict(app_model, chip_dict)


def export_hycdes(action, param, app_model):
    del action, param

    chip_dict = app_model.chip_dict

    try:
        savecode = chipcalc.hycdes.from_chip_dict(chip_dict)
    except ValueError as exc:
        show_error(exc)
        return

    Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD).set_text(savecode, -1)

    dialog = Gtk.MessageDialog(
        text='hycdes.com savecode',
        secondary_text="The savecode should be copied to your chipboard.",
        buttons=Gtk.ButtonsType.CLOSE,
        modal=True,
        destroy_with_parent=True)

    entry = Gtk.Entry(text=savecode, editable=False)
    entry.select_region(0, -1)
    dialog.get_content_area().add(entry)

    try:
        dialog.show_all()
        dialog.run()
    finally:
        dialog.destroy()


def _iter_chips(chips):
    it = chips.get_iter_first()
    while it is not None:
        yield it, chips.get_value(it, 0)
        it = chips.iter_next(it)


def _format_stars_scale(scale, value):
    return '★' * int(value)


def _select_stars(**kwargs):
    dialog = Gtk.MessageDialog(
        message_type=Gtk.MessageType.QUESTION,
        buttons=Gtk.ButtonsType.OK_CANCEL,
        modal=True,
        destroy_with_parent=True,
        **kwargs)

    scale = Gtk.Scale(orientation=Gtk.Orientation.HORIZONTAL,
                      adjustment=Gtk.Adjustment(lower=2, upper=5, value=5,
                                                step_increment=1),
                      digits=0)
    scale.connect('format_value', _format_stars_scale)

    box = dialog.get_content_area()
    box.add(scale)

    try:
        dialog.show_all()
        response = dialog.run()
        stars = int(scale.get_value())
    finally:
        dialog.destroy()

    if response != Gtk.ResponseType.OK:
        return None

    return stars


def _format_level_scale(scale, value):
    return f'+{value:>.{scale.get_digits()}f}'


def _select_level(**kwargs):
    dialog = Gtk.MessageDialog(
        message_type=Gtk.MessageType.QUESTION,
        buttons=Gtk.ButtonsType.OK_CANCEL,
        modal=True,
        destroy_with_parent=True,
        **kwargs)

    scale = Gtk.Scale(orientation=Gtk.Orientation.HORIZONTAL,
                      adjustment=Gtk.Adjustment(lower=0, upper=20, value=20,
                                                step_increment=1),
                      digits=0)
    scale.connect('format_value', _format_level_scale)

    box = dialog.get_content_area()
    box.add(scale)

    try:
        dialog.show_all()
        response = dialog.run()
        level = int(scale.get_value())
    finally:
        dialog.destroy()

    if response != Gtk.ResponseType.OK:
        return None

    return level


def fill_stars(action, param, app_model):
    """Fills stars."""
    del action, param

    it = app_model.chip_dict.get_iter(app_model.color)
    if it is None:
        return
    chips = app_model.chips.get_value(it, 1)

    stars = _select_stars(text='Select stars')
    if stars is None:
        return

    for it, chip in _iter_chips(chips):
        if 'stars' in chip.stats:
            continue

        new_chip = dataclasses.replace(chip, stars=stars)
        chips.set_row(it, (new_chip,))


class _FillBlocksContext:

    RESPONSE_CONFIRM = 0
    RESPONSE_IGNORE = 1
    RESPONSE_IGNORE_ALL = 2

    def __init__(self):
        self._ignore_all = False

    def _create_candidate_combobox(self, candidates):
        del self

        model = Gtk.ListStore(str, object)
        for chip in reversed(candidates):
            stars = '★' * chip['stars']
            level = chip['level']
            description = f'{stars} +{level}'
            model.append((description, chip))

        combobox = Gtk.ComboBox(model=model, active=0)

        renderer = Gtk.CellRendererText()
        combobox.pack_start(renderer, False)
        combobox.add_attribute(renderer, 'text', 0)

        return combobox

    def _get_selected_candidate(self, combobox):
        del self

        model = combobox.get_model()
        active_iter = combobox.get_active_iter()
        return model.get_value(active_iter, 1)

    def fill_blocks_for_chip(self, chip):
        """Returns chip with db, pb, ab and rb filled."""

        candidates = gfl.get_possible_full_chips(chip)

        if not candidates:
            show_error(ValueError(f'invalid chip {chip}'))
            return None

        if len(candidates) == 1:
            return candidates[0]

        if self._ignore_all:
            return None

        dialog = Gtk.MessageDialog(
            text=f'What stars and level should {chip} be?',
            secondary_text='Confirm: use the selected stars and level\n'
                           'Ignore: leave this chip unchanged\n'
                           'Ignore All: leave all ambiguous chips unchanged',
            message_type=Gtk.MessageType.QUESTION,
            modal=True,
            destroy_with_parent=True)
        dialog.add_buttons(
            'Confirm', _FillBlocksContext.RESPONSE_CONFIRM,
            'Ignore', _FillBlocksContext.RESPONSE_IGNORE,
            'Ignore All', _FillBlocksContext.RESPONSE_IGNORE_ALL)

        combobox = self._create_candidate_combobox(candidates)

        box = dialog.get_content_area()
        box.add(combobox)

        try:
            dialog.show_all()
            response = dialog.run()
            chip = self._get_selected_candidate(combobox)
        finally:
            dialog.destroy()

        if response == _FillBlocksContext.RESPONSE_CONFIRM:
            return chip

        if response == _FillBlocksContext.RESPONSE_IGNORE:
            return None

        self._ignore_all = True
        return None


def fill_blocks(action, param, app_model):
    """Fills db, pb, ab, rb."""
    del action, param

    it = app_model.chip_dict.get_iter(app_model.color)
    if it is None:
        return
    chips = app_model.chips.get_value(it, 1)

    context = _FillBlocksContext()

    for it, chip in _iter_chips(chips):
        new_chip = context.fill_blocks_for_chip(chip)
        if new_chip is not None:
            chips.set_row(it, (new_chip,))


def strengthen_chips(action, param, app_model):
    """Strengthen chips to a specific level."""
    del action, param

    it = app_model.chip_dict.get_iter(app_model.color)
    if it is None:
        return
    chips = app_model.chips.get_value(it, 1)

    level = _select_level(
        text='To which level should the chips be strengthened?',
        secondary_text="Chips with higher levels won't be affected.")
    if level is None:
        return

    for it, chip in _iter_chips(chips):
        if chip['level'] >= level:
            continue

        try:
            new_chip = gfl.with_level(chip, level)
        except ValueError as exc:
            show_error(exc)
            break

        chips.set_row(it, (new_chip,))

def get_action_group(app_model):
    """Returns supported actions."""

    agroup = Gio.SimpleActionGroup()

    agroup.add_action_entries([
        ('solve', solve),
        ('import_text', import_text),
        ('export_text', export_text),
        ('import_hycdes', import_hycdes),
        ('export_hycdes', export_hycdes),
        ('fill_stars', fill_stars),
        ('fill_blocks', fill_blocks),
        ('strengthen_chips', strengthen_chips),
    ], app_model)

    return agroup
