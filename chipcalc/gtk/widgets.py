"""Combobox selecting an HOC."""
import colorsys
import dataclasses

import gi
gi.require_version('Gtk', '3.0')
gi.require_foreign('cairo')
from gi.repository import Gtk, GObject, Gdk, Pango

import chipcalc.model as chipmodel
import chipcalc.gtk.model as gtkmodel

class HOCComboBox(Gtk.ComboBox):
    """Selects shape from of a list of HOC."""

    def __init__(self):
        Gtk.ComboBox.__init__(self)

        self._app_model = None

        self.set_model(HOCComboBox._get_hoc_list())

        renderer = Gtk.CellRendererText()
        self.pack_start(renderer, True)
        self.add_attribute(renderer, 'text', 0)

    @GObject.Property(type=gtkmodel.AppModel)
    def app_model(self):
        """The associated AppModel."""
        return self._app_model

    @app_model.setter
    def set_app_model(self, app_model):
        if self._app_model is not None:
            raise RuntimeError('replacing app_model is not implemented')

        self._app_model = app_model
        self._set_area(app_model.area)

        self.connect('changed', self._on_changed)
        app_model.connect('notify::area', self._on_notify_area)

    def _set_area(self, area):
        model = self.get_model()
        it = model.get_iter_first()
        while it is not None:
            if model.get_value(it, 1) == area:
                break
            it = model.iter_next(it)
        self.set_active_iter(it)

    def _on_changed(self, combo_box):
        del combo_box

        active_iter = self.get_active_iter()
        if active_iter is None:
            return

        self._app_model.area = self.get_model().get_value(active_iter, 1)

    def _on_notify_area(self, app_model, area):
        del area
        self._set_area(app_model.area)

    @staticmethod
    def _create_area(text):
        clean_text = '/'.join(line.strip() for line in text.strip().split('\n'))
        return chipmodel.Shape(clean_text)

    @staticmethod
    def _get_hoc_list():
        list_store = Gtk.ListStore(str, object)
        list_store.append(('BGM-71 ★★★★★', HOCComboBox._create_area('''
            xxxxxx
            xxxxxx
            xxxxxx
            xxxxxx
            xxxxxx
            xxxxxx
        ''')))
        list_store.append(('AGS-30 ★★★★★', HOCComboBox._create_area('''
            ..xx....
            .xxxx...
            xxxxxx..
            xxxxxxx.
            .xxxxxxx
            ..xxxxxx
            ...xxxx.
            ....xx..
        ''')))
        list_store.append(('2B-14 ★★★★★', HOCComboBox._create_area('''
            ..x..x..
            .xxxxxx.
            xxxxxxxx
            xxxxxxxx
            .xxxxxx.
            ..x..x..
        ''')))
        list_store.append(('M2 ★★★★★', HOCComboBox._create_area('''
            xxx....x
            .xxx..xx
            ..xx.xxx
            ..xxxxx.
            .xxxxx..
            xxx.xx..
            xx..xxx.
            x....xxx
        ''')))
        list_store.append(('AT4 ★★★★★', HOCComboBox._create_area('''
            ...xx...
            ..xxxx..
            .xxxxxx.
            xxx..xxx
            xxx..xxx
            .xxxxxx.
            ..xxxx..
            ...xx...
        ''')))
        return list_store

class AreaTextView(Gtk.TextView):
    """Shows and sets shape as ASCII arts."""
    def __init__(self):
        Gtk.TextView.__init__(self)
        self._app_model = None
        self._current_area = chipmodel.Shape()

    @GObject.Property(type=gtkmodel.AppModel)
    def app_model(self):
        """The associated AppModel."""
        return self._app_model

    @app_model.setter
    def set_app_model(self, app_model):
        if self._app_model is not None:
            raise RuntimeError('replacing app_model is not implemented')

        self._app_model = app_model
        self._set_area(app_model.area)

        self.get_buffer().connect('notify::text', self._on_notify_text)
        app_model.connect('notify::area', self._on_notify_area)

    def _set_area(self, area):
        if area == self._current_area:
            return
        self.get_buffer().set_text(str(area).replace('/', '\n'))

    def _on_notify_text(self, text_buffer, text):
        del text_buffer, text

        text_buffer = self.get_buffer()
        text = text_buffer.get_text(text_buffer.get_start_iter(),
                                    text_buffer.get_end_iter(), False)
        clean_text = '/'.join(line.strip() for line in text.strip().split('\n'))

        try:
            self._current_area = chipmodel.Shape(clean_text)
        except ValueError:
            self._current_area = None
            # TODO report error to user
            return

        self._app_model.area = self._current_area

    def _on_notify_area(self, app_model, area):
        del area
        self._set_area(app_model.area)

class StatsTreeView(Gtk.TreeView):
    """Shows and edits solutions constraints in a table."""
    def __init__(self):
        Gtk.TreeView.__init__(self)
        self._app_model = None

        renderer_stat = Gtk.CellRendererText()
        renderer_has_min = Gtk.CellRendererToggle()
        renderer_min = Gtk.CellRendererText(editable=True)
        renderer_has_max = Gtk.CellRendererToggle()
        renderer_max = Gtk.CellRendererText(editable=True)

        column_stat = Gtk.TreeViewColumn()
        column_stat.set_title('Stat')
        column_stat.set_expand(True)
        column_stat.pack_start(renderer_stat, True)
        column_stat.add_attribute(renderer_stat, 'text', 0)

        column_min = Gtk.TreeViewColumn()
        column_min.set_title('Min')
        column_min.pack_start(renderer_has_min, False)
        column_min.add_attribute(renderer_has_min, 'active', 1)
        column_min.pack_start(renderer_min, True)
        column_min.add_attribute(renderer_min, 'sensitive', 1)
        column_min.add_attribute(renderer_min, 'text', 2)

        column_max = Gtk.TreeViewColumn()
        column_max.set_title('Max')
        column_max.pack_start(renderer_has_max, False)
        column_max.add_attribute(renderer_has_max, 'active', 3)
        column_max.pack_start(renderer_max, True)
        column_max.add_attribute(renderer_max, 'sensitive', 3)
        column_max.add_attribute(renderer_max, 'text', 4)

        self.append_column(column_stat)
        self.append_column(column_min)
        self.append_column(column_max)

        renderer_min.connect('edited', self._on_min_edited)
        renderer_max.connect('edited', self._on_max_edited)
        renderer_has_min.connect('toggled', self._on_has_min_toggled)
        renderer_has_max.connect('toggled', self._on_has_max_toggled)

    def _on_has_min_toggled(self, cell, path_string):
        del cell
        if self._app_model is None:
            return
        row = self._app_model.stats[path_string]
        row[1] = not row[1]

    def _on_min_edited(self, cell, path_string, new_text):
        del cell
        if self._app_model is None:
            return
        row = self._app_model.stats[path_string]
        row[2] = int(new_text)

    def _on_has_max_toggled(self, cell, path_string):
        del cell
        if self._app_model is None:
            return
        row = self._app_model.stats[path_string]
        row[3] = not row[3]

    def _on_max_edited(self, cell, path_string, new_text):
        del cell
        if self._app_model is None:
            return
        row = self._app_model.stats[path_string]
        row[4] = int(new_text)

    @GObject.Property(type=gtkmodel.AppModel)
    def app_model(self):
        "The associated AppModel."
        return self._app_model

    @app_model.setter
    def set_app_model(self, app_model):
        if self._app_model is not None:
            raise RuntimeError('replacing app_model is not implemented')

        self._app_model = app_model

        self.set_model(app_model.stats)

class ColorComboBox(Gtk.ComboBox):
    """Selects color of chips to use."""

    def __init__(self):
        Gtk.ComboBox.__init__(self)
        self._app_model = None

    def _set_color(self, color):
        model = self.get_model()

        it = self._app_model.chip_dict.get_iter(color)
        self.set_active_iter(it)

        if it is None:
            self.get_child().set_text(color)

    @GObject.Property(type=gtkmodel.AppModel)
    def app_model(self):
        """The associated AppModel."""
        return self._app_model

    @app_model.setter
    def set_app_model(self, app_model):
        if self._app_model is not None:
            raise RuntimeError('replacing app_model is not implemented')

        self._app_model = app_model
        self.set_model(app_model.chips)
        self._set_color(app_model.color)

        self.connect('changed', self._on_changed)
        app_model.connect('notify::color', self._on_notify_color)

    def _on_changed(self, combo_box):
        del combo_box
        it = self.get_active_iter()
        if it is not None:
            color = self.get_model().get_value(it, 0)
        else:
            color = self.get_child().get_text()
        self._app_model.color = color

    def _on_notify_color(self, app_model, color):
        del color
        self._set_color(app_model.color)

class ChipsTreeView(Gtk.TreeView):
    """Shows and edits chips in a table."""
    STATS = ('d', 'p', 'a', 'r', 'db', 'pb', 'ab', 'rb', 'stars', 'level')

    def __init__(self):
        Gtk.TreeView.__init__(self)
        self._app_model = None

        # shape columns
        renderer = Gtk.CellRendererText(editable=True)
        renderer.connect('edited', self._on_shape_edited)

        column = Gtk.TreeViewColumn(cell_renderer=renderer)
        column.set_title('Shape')
        column.set_cell_data_func(renderer, ChipsTreeView._chip_area_cdf)
        self.append_column(column)

        # stat columns
        for stat in ChipsTreeView.STATS:
            renderer = Gtk.CellRendererText(editable=True, xalign=1.0)
            renderer.connect('edited', self._on_stat_edited, stat)

            column = Gtk.TreeViewColumn(cell_renderer=renderer)
            column.set_title(stat)
            column.set_cell_data_func(renderer, ChipsTreeView._chip_stat_cdf,
                                      stat)
            self.append_column(column)

        # other stats
        renderer = Gtk.CellRendererText(editable=True)
        renderer.connect('edited', self._on_multi_stat_edited)

        column = Gtk.TreeViewColumn(cell_renderer=renderer)
        column.set_cell_data_func(renderer, ChipsTreeView._chip_other_stats_cdf)
        self.append_column(column)

    @staticmethod
    def _chip_area_cdf(column, renderer, model, it, user_data):
        del column, user_data
        chip = model.get_value(it, 0)
        renderer.set_property('text', str(chip.shape))

    @staticmethod
    def _chip_stat_cdf(column, renderer, model, it, stat):
        del column
        chip = model.get_value(it, 0)
        value = chip.stats.get(stat)
        renderer.set_property('text', str(value) if value is not None else '')

    @staticmethod
    def _chip_other_stats_cdf(column, renderer, model, it, user_data):
        del column, user_data
        chip = model.get_value(it, 0)

        stats = []
        for stat, value in chip.stats.items():
            if stat not in ChipsTreeView.STATS:
                stats.append(f'{stat}={value}')

        renderer.set_property('text', ' '.join(stats))

    @GObject.Property(type=gtkmodel.AppModel)
    def app_model(self):
        """The associated AppModel."""
        return self._app_model

    @app_model.setter
    def set_app_model(self, app_model):
        if self._app_model is not None:
            raise RuntimeError('replacing app_model is not implemented')

        self._app_model = app_model
        self._update_model()

        app_model.connect('notify::color', self._on_notify_color)

    def _on_shape_edited(self, cell, path_string, new_text):
        del cell
        # TODO report error to user
        old_chip = self.get_model()[path_string][0]
        new_shape = chipmodel.Shape(new_text)
        new_chip = dataclasses.replace(old_chip, shape=new_shape)
        self.get_model()[path_string][0] = new_chip

    def _on_stat_edited(self, cell, path_string, new_text, stat):
        del cell
        # TODO report error to user
        old_chip = self.get_model()[path_string][0]
        if new_text:
            new_value = int(new_text)
            new_chip = dataclasses.replace(old_chip, **{stat: new_value})
        else:
            stats = dict(old_chip.stats)
            del stats[stat]
            new_chip = dataclasses.replace(old_chip, stats=stats)
        self.get_model()[path_string][0] = new_chip

    def _on_multi_stat_edited(self, cell, path_string, new_text):
        del cell
        # TODO report error to user
        old_chip = self.get_model()[path_string][0]

        new_stats = {}
        for stat, value in old_chip.stats.items():
            if stat in ChipsTreeView.STATS:
                new_stats[stat] = value

        for entry in new_text.split():
            stat, value = entry.rsplit('=', 1)
            new_stats[stat] = int(value)

        new_chip = dataclasses.replace(old_chip, stats=new_stats)
        self.get_model()[path_string][0] = new_chip

    def _on_notify_color(self, app_model, color):
        del app_model, color
        self._update_model()

    def _update_model(self):
        app_model = self._app_model
        color = app_model.color
        it = app_model.chip_dict.get_iter(color)
        if it is None:
            self.set_model(None)
        else:
            new_model = app_model.chips.get_value(it, 1)
            self.set_model(new_model)

class SolutionCountHeaderBar(Gtk.HeaderBar):
    def __init__(self, **kwargs):
        self._solver_model = None
        Gtk.Label.__init__(self, **kwargs)

    @GObject.Property(flags=GObject.ParamFlags.READWRITE |
                      GObject.ParamFlags.CONSTRUCT_ONLY,
                      type=gtkmodel.SolverModel)
    def solver_model(self):
        """The associated SolverModel."""
        return self._solver_model

    @solver_model.setter
    def set_solver_model(self, solver_model):
        if self._solver_model is not None:
            raise RuntimeError('replacing solver_model is not implemented')

        self._solver_model = solver_model
        self._update_row_count()

        solver_model.solutions.connect('row-inserted', self._on_row_inserted)
        solver_model.solutions.connect('row-deleted', self._on_row_deleted)

    def _update_row_count(self):
        rows = self._solver_model.solutions.iter_n_children(None)
        if rows == 0:
            self.set_title('No Solution')
        elif rows == 1:
            self.set_title('1 Solution')
        else:
            self.set_title(f'{rows} Solutions')

    def _on_row_inserted(self, tree_model, path, it):
        del tree_model, path, it
        self._update_row_count()

    def _on_row_deleted(self, tree_model, path):
        del tree_model, path
        self._update_row_count()

class SolutionsTreeView(Gtk.TreeView):
    """A Gtk.TreeView displaying list of solutions."""

    def __init__(self, **kwargs):
        self._solver_model = None
        Gtk.TreeView.__init__(self, **kwargs)

    @GObject.Property(flags=GObject.ParamFlags.READWRITE |
                      GObject.ParamFlags.CONSTRUCT_ONLY,
                      type=gtkmodel.SolverModel)
    def solver_model(self):
        """The associated SolverModel."""
        return self._solver_model

    @solver_model.setter
    def set_solver_model(self, solver_model):
        self._solver_model = solver_model

        self.set_model(solver_model.solutions)

        # chip ids columns
        renderer = Gtk.CellRendererText()

        column = Gtk.TreeViewColumn(title='Chips', cell_renderer=renderer)
        column.set_cell_data_func(renderer, self._solution_chips_cdf)
        self.append_column(column)

        # stats columns
        for stat_index, stat in enumerate(solver_model.param.stats, start=1):
            renderer = Gtk.CellRendererText()

            # column.set_title('__len__') doesn't work as expected.
            title = Gtk.Label(label=stat)
            title.show_all()

            column = Gtk.TreeViewColumn(cell_renderer=renderer, text=stat_index)
            column.set_widget(title)
            column.set_sort_column_id(stat_index)
            self.append_column(column)

        self.get_selection().connect('changed', self._on_changed)

    @staticmethod
    def _solution_chips_cdf(column, renderer, model, it, param):
        del column, param
        solution = model.get_value(it, 0)
        value = ','.join(str(entry[0]) for entry in solution)
        renderer.set_property('text', value)

    def _on_changed(self, selection):
        model, it = selection.get_selected()
        if it is None:
            self._solver_model.focused_ref = None
        else:
            path = model.get_path(it)
            ref = Gtk.TreeRowReference(model, path)
            self._solver_model.focused_ref = ref

class SolutionAreaView(Gtk.DrawingArea):
    """Displaying a solution on an area."""

    def __init__(self, **kwargs):
        self._solver_model = None
        Gtk.DrawingArea.__init__(self, **kwargs)

    @GObject.Property(flags=GObject.ParamFlags.READWRITE |
                      GObject.ParamFlags.CONSTRUCT_ONLY,
                      type=gtkmodel.SolverModel)
    def solver_model(self):
        """The associated SolverModel."""
        return self._solver_model

    @solver_model.setter
    def set_solver_model(self, solver_model):
        if self._solver_model is not None:
            raise RuntimeError('replacing solver_model is not implemented')

        self._solver_model = solver_model
        self.queue_draw()

        solver_model.connect('notify::focused-ref', self._on_notify_focus)

    def _on_notify_focus(self, solver_model, area):
        del solver_model, area
        self.queue_draw()

    def do_get_request_mode(self):
        return Gtk.SizeRequestMode.HEIGHT_FOR_WIDTH

    def do_get_preferred_width(self):
        return 0, 200

    def do_get_preferred_height_for_width(self, width):
        return 0, width

    def do_draw(self, ctx):
        if self._solver_model is None:
            return

        area = self._solver_model.param.area
        if not area:
            return

        ctx.scale(self.get_allocated_width() / area.width,
                  self.get_allocated_height() / area.height)

        ctx.set_source_rgb(1, 1, 1)  # white
        self._fill_shape(ctx, area)

        focused_row = self._solver_model.focused_row
        if focused_row is not None:
            solution = self._solver_model.row_to_solution(focused_row)
            chips = solution.chips(self._solver_model.param.chips)

            for i, chip in enumerate(chips):
                ctx.set_source_rgb(*self.get_color(i, len(chips)))
                self._fill_shape(ctx, chip.shape)

    @staticmethod
    def get_color(i, n):
        """Returns the i-th of n visually distinct colors."""
        hue = i / n
        saturation = value = 1 - (i % 2) * 0.25
        return colorsys.hsv_to_rgb(hue, saturation, value)

    @staticmethod
    def _fill_shape(ctx, shape):
        for y, x in shape:
            ctx.rectangle(x, y, 1, 1)
        ctx.fill()

class SolutionView(Gtk.TreeView):
    """Displaying chips of a single solution."""

    def __init__(self, **kwargs):
        self._solver_model = None
        Gtk.TreeView.__init__(self, **kwargs)

    @GObject.Property(flags=GObject.ParamFlags.READWRITE |
                      GObject.ParamFlags.CONSTRUCT_ONLY,
                      type=gtkmodel.SolverModel)
    def solver_model(self):
        """The associated SolverModel."""
        return self._solver_model

    @solver_model.setter
    def set_solver_model(self, solver_model):
        if self._solver_model is not None:
            raise RuntimeError('replacing solver_model is not implemented')

        self._solver_model = solver_model

        # chip id column
        renderer = Gtk.CellRendererText()

        column = Gtk.TreeViewColumn(title='ID')
        column.pack_start(renderer, False)
        column.set_cell_data_func(renderer, self._chip_id_cdf)
        self.append_column(column)

        # stats columns
        for stat in solver_model.param.stats:
            renderer = Gtk.CellRendererText()

            # column.set_title('__len__') doesn't work as expected.
            title = Gtk.Label(label=stat)
            title.show_all()

            column = Gtk.TreeViewColumn()
            column.set_widget(title)
            column.pack_start(renderer, False)
            column.set_cell_data_func(renderer, self._chip_stat_cdf, stat)
            self.append_column(column)

        self._update_model()
        solver_model.connect('notify::focused-ref', self._on_notify_focus)

    @staticmethod
    def _get_background_rgba(model, it):
        index = model.get_path(it).get_indices()[0]
        num = model.iter_n_children(None)
        return Gdk.RGBA(*SolutionAreaView.get_color(index, num))

    @staticmethod
    def _chip_id_cdf(column, renderer, model, it, param):
        del column, param
        chip_id = model.get_value(it, 0)
        renderer.set_property('text', str(chip_id))
        renderer.set_property('cell_background_rgba',
                              SolutionView._get_background_rgba(model, it))

    def _chip_stat_cdf(self, column, renderer, model, it, stat):
        del column
        entry = chipmodel.SolutionEntry(*model.get(it, 0, 1, 2, 3))
        chips = self._solver_model.param.chips
        value = entry.get_stat(stat, chips)
        renderer.set_property('text', str(value))
        renderer.set_property('cell_background_rgba',
                              SolutionView._get_background_rgba(model, it))

    def _update_model(self):
        focused_row = self._solver_model.focused_row
        self.set_model(focused_row[0] if focused_row is not None else None)

    def _on_notify_focus(self, solver_model, area):
        del solver_model, area
        self._update_model()

def register_all():
    """Registers all widgets."""
    # Don't need to do anything as long as this module is imported.
