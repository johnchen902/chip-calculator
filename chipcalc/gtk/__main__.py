"""GUI main module."""
import pkgutil

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import chipcalc.gtk.actions as gtkactions
import chipcalc.gtk.model as gtkmodel
import chipcalc.gtk.widgets as gtkwidgets
import chipcalc.model as chipmodel

def create_model():
    """Creates a sane (for user) default AppModel."""
    app_model = gtkmodel.AppModel()

    app_model.area = chipmodel.Shape('/'.join(['xxxxxx'] * 6))

    for stat in ['d', 'p', 'a', 'r', 'db', 'pb', 'ab', 'rb',
                 '__len__', '__rotate__']:
        app_model.stats_map[stat] = (None, None)

    app_model.color = 'blue'
    app_model.chip_dict['blue'] = []
    app_model.chip_dict['orange'] = []

    return app_model

def main():
    """GUI main function."""
    gtkwidgets.register_all()

    app_model = create_model()

    builder = Gtk.Builder.new()
    builder.expose_object('app_model', app_model)
    builder_xml = pkgutil.get_data('chipcalc.gtk', 'main.ui')
    builder.add_from_string(builder_xml.decode('utf8'))

    main_window = builder.get_object('main-window')
    main_window.connect("destroy", Gtk.main_quit)
    main_window.show_all()

    main_window.insert_action_group('app_model',
                                    gtkactions.get_action_group(app_model))

    Gtk.main()

if __name__ == '__main__':
    main()
