"""The minimal data structure needed from the problem"""
from dataclasses import dataclass, replace
import collections
import keyword
import typing

@dataclass(frozen=True)
class Shape(collections.abc.Set):
    """Shape of a chip or a HOC.

    This is a frozen set of (row, col).
    """

    points: typing.AbstractSet[typing.Tuple[int, int]]

    def __init__(self, points=frozenset()):
        if isinstance(points, str):
            if not set(points) <= set('.x/'):
                raise ValueError(f'Illegal characters: {set(points) - set(".x/")}')
            points = ((i, j) for i, line in enumerate(points.split('/'))
                      for j, c in enumerate(line.strip())
                      if c == 'x')

        object.__setattr__(self, 'points', frozenset(points))

    @property
    def width(self):
        """The distance between y-axis and the right of rightmost block"""
        return max((c + 1 for r, c in self.points), default=0)

    @property
    def height(self):
        """The distance between x-axis and the bottom of bottom block"""
        return max((r + 1 for r, c in self.points), default=0)

    def __len__(self):
        return len(self.points)

    def __contains__(self, point):
        return point in self.points

    def __iter__(self):
        return iter(self.points)

    def __str__(self):
        if not self:
            return '.'
        return '/'.join(''.join('x' if (row, col) in self else '.'
                                for col in range(self.width)).rstrip('.')
                        for row in range(self.height))

    def __repr__(self):
        if not self:
            return f'{self.__class__.__name__}()'
        return f'{self.__class__.__name__}({str(self)!r})'

    def shift(self, down, right):
        """Return a shifted copy of this shape"""
        return Shape((r + down, c + right) for r, c in self.points)

    def shift_right(self, n=1):
        """Return a copy of this shape shifted right by n blocks"""
        return Shape((r, c + n) for r, c in self.points)

    def shift_down(self, n=1):
        """Return a copy of this shape shifted down by n blocks"""
        return Shape((r + n, c) for r, c in self.points)

    def translations(self, area):
        """Return all translations of this shape in area"""
        for i in range(area.height - self.height + 1):
            for j in range(area.width - self.width + 1):
                shape = self.shift(i, j)
                if shape <= area:
                    yield shape

    def shift_normal(self):
        """Return a copy of this shape shift to top left"""
        left = min((c for r, c in self.points), default=0)
        up = min((r for r, c in self.points), default=0)
        return self.shift(-up, -left)

    def rotate_clockwise(self):
        """Return a copy of this shape rotated clockwise"""
        return Shape((c, self.height - 1 - r) for r, c in self.points)

    def rotations(self):
        """Return all unique rotations of this shape"""
        yield self
        shape = self
        while True:
            shape = shape.rotate_clockwise()
            if shape == self:
                break
            yield shape

    def variants(self, area):
        """Yields all traslations and rotations fitting in area.

        Return type is [(shift_x, shift_y, rotate, Shape)].
        """

        for rotate, rotated_chip in enumerate(self.rotations()):
            for shift_y in range(area.height - rotated_chip.height + 1):
                for shift_x in range(area.width - rotated_chip.width + 1):
                    shifted_chip = rotated_chip.shift(shift_y, shift_x)
                    if shifted_chip <= area:
                        yield shift_x, shift_y, rotate, shifted_chip

@dataclass
class Chip:
    """A chip, which has a shape and some stats"""
    shape: Shape
    stats: typing.Mapping[str, int]

    def __init__(self, shape, stats=None, **kwargs):
        if stats is None:
            stats = dict(**kwargs)
        else:
            stats = dict(stats, **kwargs)

        self.shape = shape
        self.stats = stats

    def __getitem__(self, key):
        return self.stats.get(key, 0)

    def __str__(self):
        return str(self.shape) + ''.join(f' {key}={value}'
                                         for key, value in self.stats.items())

    def __repr__(self):
        if all(key.isidentifier() and not keyword.iskeyword(key)
               and key not in {'shape', 'stats'}
               for key in self.stats):
            stats = ''.join(f', {key}={value!r}'
                            for key, value in self.stats.items())
            return f'{self.__class__.__name__}({self.shape!r}{stats})'
        return f'{self.__class__.__name__}({self.shape!r}, {self.stats!r})'

    @staticmethod
    def parse(text):
        """Inverse of Chip.__str__"""
        shape, *args = text.split()

        stats = {}
        for arg in args:
            key, value = arg.rsplit('=', 1)
            stats[key] = int(value)

        return Chip(Shape(shape), stats)

@dataclass
class Parameter:
    """Parameters to a solver."""
    area: Shape
    chips: typing.Sequence[Chip]
    stats: typing.Mapping[str, typing.Tuple[int, int]]

SolutionEntry = collections.namedtuple(
    'SolutionEntry',
    ('chip_id', 'shift_x', 'shift_y', 'rotate'))

def _get_chip(entry, chips):
    chip = chips[entry.chip_id]

    shape = chip.shape
    for _ in range(entry.rotate):
        shape = shape.rotate_clockwise()
    shape = shape.shift(entry.shift_y, entry.shift_x)

    return replace(chip, shape=shape)

def _get_stat(entry, stat, chips):
    if stat == '__len__':
        return len(_get_chip(entry, chips).shape)
    if stat == '__rotate__':
        return 1 if entry.rotate else 0
    return _get_chip(entry, chips)[stat]

SolutionEntry.get_chip = _get_chip
SolutionEntry.get_stat = _get_stat

class Solution(list):
    """A solution i.e. a list of SolutionEntry."""

    def chips(self, chips):
        """Returns chips of this solution."""
        return [entry.get_chip(chips) for entry in self]

    def chip_ids(self):
        """Returns chip ids of this solution."""
        return [entry.chip_id for entry in self]

    def stats(self, chips):
        """Returns total stats of this solution."""
        stats = {}
        for entry in self:
            for stat, value in chips[entry.chip_id].stats.items():
                stats[stat] = stats.get(stat, 0) + value
        return stats

    def get_stat(self, stat, chips):
        """Returns total specified stat of this solution."""
        return sum(entry.get_stat(stat, chips) for entry in self)

    def num_rotated(self):
        """Returns total number of rotated chips of this solution."""
        return sum(bool(entry.rotate) for entry in self)
