"""Logic about chips in Girls' Frontline."""

import dataclasses
import math

from chipcalc.model import Shape

_CLASS1_SHAPES = frozenset(shape for base_shape in [
    Shape('.xx/xx/.x'),
    Shape('xx/.xx/.x'),
    Shape('xxx/..xx'),
    Shape('.xxx/xx'),
    Shape('x/xxx/x'),
    Shape('..x/.xx/xx'),
    Shape('.x/xxx/.x'),
    Shape('..x/xxxx'),
    Shape('.x/xxxx'),
] for shape in base_shape.rotations())

_DENSITY_TABLE = (
    (0.32, 0.32, 0.36, 0.4),
    (0.48, 0.48, 0.56, 0.6),
    (0.6, 0.64, 0.72, 0.8),
    (0.76, 0.8, 0.92, 1.0),
)

_STAT_COEFF = {
    'd': 4.4,
    'p': 12.7,
    'a': 7.1,
    'r': 5.7,
}


def get_shape_density(shape, stars):
    """Returns the density of a shape."""

    if not 2 <= stars <= 5:
        raise ValueError(f'invalid number of stars: {stars}')

    blocks = 6 if shape in _CLASS1_SHAPES else len(shape)
    if not 3 <= blocks <= 6:
        raise ValueError(f'invalid shape: {shape}')

    return _DENSITY_TABLE[stars - 2][blocks - 3]


def get_level_multiplier(level):
    """Returns multiplier for the given level."""
    if level <= 10:
        return 1.0 + level * 0.08
    return 1.1 + level * 0.07


def _get_value(stat_coeff, blocks, density, level):
    basevalue = math.ceil(stat_coeff * blocks * density)
    return math.ceil(basevalue * get_level_multiplier(level))


def _get_blocks(stat_coeff, value, density, level):
    base_value = math.floor(value / get_level_multiplier(level))
    blocks = math.floor(base_value / (stat_coeff * density))
    if _get_value(stat_coeff, blocks, density, level) != value:
        raise ValueError
    return blocks


def with_level(chip, level):
    """Returns a copy of (full) chip with new level."""

    try:
        stars = chip.stats['stars']
    except KeyError as exc:
        raise ValueError(f'no stars in chip {chip}') from exc

    density = get_shape_density(chip.shape, stars)

    new_attribs = {}
    sum_blocks = 0

    for stat in ['d', 'p', 'a', 'r']:
        blocks = chip[stat + 'b']
        sum_blocks += blocks

        value = _get_value(_STAT_COEFF[stat], blocks, density, level)

        new_attribs[stat] = value

    if sum_blocks != len(chip.shape):
        if sum_blocks == 0:
            raise ValueError(f'chip {chip} has no blocks')
        raise ValueError(f'invalid chip {chip}')

    return dataclasses.replace(chip, level=level, **new_attribs)


def fill_blocks(chip):
    """Returns a chip with blocks information."""

    try:
        stars = chip.stats['stars']
    except KeyError as exc:
        raise ValueError(f'no stars in chip {chip}') from exc

    try:
        level = chip.stats['level']
    except KeyError as exc:
        raise ValueError(f'no level in chip {chip}') from exc

    density = get_shape_density(chip.shape, stars)

    new_attribs = {}
    sum_blocks = 0

    for stat in ['d', 'p', 'a', 'r']:
        try:
            blocks = _get_blocks(_STAT_COEFF[stat], chip[stat], density, level)
        except ValueError as exc:
            raise ValueError(f'invalid chip {chip}') from exc

        new_attribs[stat + 'b'] = blocks
        sum_blocks += blocks

    if sum_blocks != len(chip.shape):
        raise ValueError(f'invalid chip {chip}')

    return dataclasses.replace(chip, **new_attribs)


def _with_possible_stars(chip):
    if 'stars' in chip.stats:
        yield chip
    else:
        for stars in range(2, 5 + 1):
            yield dataclasses.replace(chip, stars=stars)


def _with_possible_levels(chip):
    if 'level' in chip.stats:
        yield chip
    else:
        for level in range(0, 20 + 1):
            yield dataclasses.replace(chip, level=level)


def get_possible_full_chips(chip):
    """Returns possible list of chip."""

    full_chips = []

    for chip1 in _with_possible_stars(chip):
        for chip2 in _with_possible_levels(chip1):
            try:
                full_chip = fill_blocks(chip2)
            except ValueError:
                continue

            full_chips.append(full_chip)

    return full_chips
