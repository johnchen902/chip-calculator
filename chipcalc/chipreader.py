"Read/write chips with user-editable format"
import io
import collections.abc

from .model import Chip

def write_chips(chips, fobj):
    """Writes a list of chips to a file-like object."""

    # Makes sure write_chips(chip_dict, fobj) fails.
    if not isinstance(chips, collections.abc.Sequence):
        raise TypeError(f'expected a sequence, found {type(chips)}')

    for chip in chips:
        fobj.write(f'{chip}\n')

def write_chip_dict(chip_dict, fobj):
    """Writes a dict of (color, chips) to a file-like object."""

    # Makes sure write_chip_dict(chips, fobj) fails.
    if not isinstance(chip_dict, collections.abc.Mapping):
        raise TypeError(f'expected a mapping, found {type(chip_dict)}')

    for color, chips in chip_dict.items():
        fobj.write(f'[{color}-chips]\n')
        write_chips(chips, fobj)

def _remove_comment(line):
    semicolon = line.find(';')
    if semicolon >= 0:
        line = line[:semicolon]
    return line.strip().replace('\u2026', '...')

def read_chips(fobj):
    """Reads a list of chips from a file-like object."""

    chips = []

    for lineno, line in enumerate(fobj, start=1):
        line = _remove_comment(line)
        if not line:
            continue

        if line[0] == '[' and line[-1] == ']':
            raise ValueError(f"line {lineno}: unexpected section")

        try:
            chip = Chip.parse(line)
        except ValueError as e:
            raise ValueError(f'line {lineno}: invalid chip {line!r}') from e

        chips.append(chip)

    return chips

def read_chip_dict(fobj):
    """Reads a dict of (color, chips) from a file-like object."""

    chip_dict = {}
    color = None

    for lineno, line in enumerate(fobj, start=1):
        line = _remove_comment(line)
        if not line:
            continue

        if line[0] == '[' and line[-1] == ']':
            # new section i.e. new color
            section_name = line[1:-1].strip()
            if not section_name.endswith('-chips'):
                raise ValueError(f"line {lineno}: section name must ends with '-chips'")

            color = section_name[:-6]
            if color in chip_dict:
                raise ValueError(f'line {lineno}: duplicate color {color!r}')
            chip_dict[color] = []
            continue

        try:
            chip = Chip.parse(line)
        except ValueError as e:
            raise ValueError(f'line {lineno}: invalid chip {line!r}') from e

        if color is None:
            raise ValueError(f'line {lineno}: no color for chip {line!r}')

        chip_dict[color].append(chip)

    return chip_dict

def format_chips(chips):
    """Formats a list of chips as string."""
    fobj = io.StringIO()
    write_chips(chips, fobj)
    return fobj.getvalue()

def format_chip_dict(chip_dict):
    """Formats a dict of (color, chips) as string."""
    fobj = io.StringIO()
    write_chip_dict(chip_dict, fobj)
    return fobj.getvalue()

def parse_chips(text):
    """Parses a list of chips from string."""
    return read_chips(io.StringIO(text))

def parse_chip_dict(text):
    """Parses a dict of (color, chips) from string."""
    return read_chip_dict(io.StringIO(text))
