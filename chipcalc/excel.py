"""Imports and exports Excel worksheet."""
import openpyxl

from .model import Shape, Chip

SHAPE_CODES = {
    1: Shape('xxx/xxx'),
    2: Shape('xxx/xx/x'),
    3: Shape('xxxx/.xx'),
    '4a': Shape('xxx/.xxx'),
    '4b': Shape('.xxx/xxx'),
    5: Shape('xx/.xx/xx'),
    6: Shape('..x/xxxx/..x'),
    7: Shape('xxxxxx'),
    8: Shape('xxxx/x..x'),
    9: Shape('xx/xxx/.x'),
    'Fa': Shape('.xx/xx/.x'),
    'Fb': Shape('xx/.xx/.x'),
    'Na': Shape('xxx/..xx'),
    'Nb': Shape('.xxx/xx'),
    'T': Shape('x/xxx/x'),
    'W': Shape('..x/.xx/xx'),
    'X': Shape('.x/xxx/.x'),
    'Ya': Shape('..x/xxxx'),
    'Yb': Shape('.x/xxxx'),
}

REVERSE_SHAPE_CODES = {shape: code for code, ref_shape in SHAPE_CODES.items()
                       for shape in ref_shape.rotations()}

WILDCARD = '*.xlsx;*.xlsm;*.xltx;*.xltm'
DEFAULT_SHEET = '\u82af\u7247\u603b\u8868'
DEFAULT_SHAPE_RANGE = 'B11:B510'
DEFAULT_STATS_RANGE = 'H11:K510'

def get_sheetnames(path):
    """Returns name of worksheets in the workbook."""
    workbook = openpyxl.load_workbook(path, read_only=True, data_only=True)
    return workbook.sheetnames

def _to_chip(row):
    (scell, ), (dcell, pcell, acell, rcell) = row

    shape = scell.value
    if not shape:
        return None

    dmg = int(dcell.value) if dcell.value else 0
    pen = int(pcell.value) if pcell.value else 0
    acc = int(acell.value) if acell.value else 0
    rel = int(rcell.value) if rcell.value else 0

    try:
        shape = SHAPE_CODES[shape]
    except KeyError:
        raise ValueError(f'Unknown shape: {shape}')

    return Chip(shape, d=dmg, p=pen, a=acc, r=rel)

def import_chips(path, sheet=DEFAULT_SHEET,
                 shape_range=DEFAULT_SHAPE_RANGE,
                 stats_range=DEFAULT_STATS_RANGE):
    """Imports chips from an Excel worksheet."""
    workbook = openpyxl.load_workbook(path, read_only=True, data_only=True)
    worksheet = workbook[sheet]

    try:
        cells = zip(worksheet[shape_range], worksheet[stats_range])
    except TypeError as exc:
        bad_range = shape_range if '#1' in str(exc) else stats_range
        raise ValueError(f'Invalid range {bad_range}')

    chips = [_to_chip(row) for row in cells]
    return [chip for chip in chips if chip is not None]

def export_chips(path, chips):
    """Exports chips to an Excel worksheet."""
    workbook = openpyxl.Workbook(write_only=True)
    worksheet = workbook.create_sheet()
    worksheet.append(['shape', 'd', 'p', 'a', 'r'])

    for chip in chips:
        try:
            shape = REVERSE_SHAPE_CODES[chip.shape]
        except KeyError:
            raise ValueError(f'Unsupported shape: {chip.shape}')
        worksheet.append([shape, chip['d'], chip['p'], chip['a'], chip['r']])

    workbook.save(path)
