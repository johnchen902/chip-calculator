"""CLI main module"""
import argparse
import sys
import traceback

import chipcalc.chipreader as chipreader
import chipcalc.model as chipmodel
import chipcalc.solver as chipsolver

def parse_prog_args():
    """Parses program arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--area', type=chipmodel.Shape, required=True,
                        help='area of the HOC')

    parser.add_argument('--chips', type=argparse.FileType(), required=True,
                        help='path to chip_dict (if color is set) or chips',
                        metavar='CHIPS_FILE')

    parser.add_argument('--color',
                        help='color of chips to use')

    parser.add_argument('--constraint', action='append', default=[],
                        help='add required constraint',
                        metavar='STAT,MINIMUM,MAXIMUM')

    parser.add_argument('--local', action='store_true',
                        help='run computation in current process')

    return parser.parse_args()

def get_stats(prog_args):
    """Returns stats from program arguments."""
    stats = {}
    for constraint in prog_args.constraint:
        stat, minimum, maximum = constraint.split(',')
        minimum = int(minimum) if minimum else None
        maximum = int(maximum) if maximum else None
        stats[stat] = minimum, maximum
    return stats

def get_chips(prog_args):
    """Returns chips from program arguments."""
    if prog_args.color is None:
        return chipreader.read_chips(prog_args.chips)

    return chipreader.read_chip_dict(prog_args.chips)[prog_args.color]

def print_header(param):
    """Prints header."""
    print('sol_id', *param.stats, sep='\t')

def get_formatted_area_char_at(area, chips, row, col):
    """Returns char (row, col) in the formatted area."""
    if (row, col) not in area:
        return '.'

    for i, chip in enumerate(chips):
        if (row, col) in chip.shape:
            return chr(ord('a') + i)

    return '_'

def get_formatted_area(area, chips):
    """Returns formatted area."""
    lines = []
    for row in range(area.height):
        line = ''
        for col in range(area.width):
            line += get_formatted_area_char_at(area, chips, row, col)
        lines.append(line.rstrip('.'))
    return '\n'.join(lines)

def print_solution(param, sol_id, solution):
    """Prints solution."""
    stats = [solution.get_stat(stat, param.chips) for stat in param.stats]
    oneline_area = get_formatted_area(param.area, solution.chips(param.chips))
    formatted_area = oneline_area.replace('\n', '/')

    print(sol_id, *stats, solution.chip_ids(), formatted_area, sep='\t')

def main():
    """CLI main function."""
    prog_args = parse_prog_args()
    stats = get_stats(prog_args)
    chips = get_chips(prog_args)

    param = chipmodel.Parameter(area=prog_args.area,
                                stats=stats,
                                chips=chips)
    print_header(param)

    if prog_args.local:
        solver = chipsolver.LocalSolver(param)
    else:
        solver = chipsolver.Solver(param)

    num_sols = 0
    for command in solver.run():
        if command[0] in [chipsolver.Command.NEW, chipsolver.Command.REPLACE]:
            solution = command[-1]
            num_sols += 1
            print_solution(param, num_sols, solution)
        elif command[0] == chipsolver.Command.ERROR:
            exc = command[1]
            traceback.print_exception(type(exc), exc, exc.__traceback__)
        else:
            print('Unknown command ignored:', command, file=sys.stderr)

if __name__ == '__main__':
    sys.exit(main())
