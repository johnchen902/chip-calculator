"""Find solutions when given parameters"""
from dataclasses import replace
import enum
import multiprocessing as mp
import queue
import sys
import threading

from ortools.sat.python import cp_model

from .model import SolutionEntry, Solution

class Command(enum.Enum):
    """Commands by solvers"""
    NEW = enum.auto()
    REPLACE = enum.auto()
    ERROR = enum.auto()
    DONE = enum.auto()

def _make_chips(param, model):  # [(var, SolutionEntry, chip)]
    chips = []

    name_counter = 0
    for chip_id, chip in enumerate(param.chips):
        shape = chip.shape
        variants = []

        for shift_x, shift_y, rotate, new_shape in shape.variants(param.area):
            new_chip = replace(chip, shape=new_shape,
                               __len__=len(shape), __rotate__=bool(rotate))

            solution_entry = SolutionEntry(chip_id, shift_x, shift_y, rotate)

            var = model.NewBoolVar(f'x_{name_counter}')
            name_counter += 1

            chips.append((var, solution_entry, new_chip))
            variants.append(var)

        model.Add(sum(variants) <= 1)

    return chips

def _compute_sum_at_pos(chips, pos):
    total = 0
    for var, _, chip in chips:
        if pos in chip.shape:
            total += var
    return total

def _compute_sum_stat(chips, stat):
    total = 0
    for var, _, chip in chips:
        value = chip[stat]
        if value:
            total += var * value
    return total

def _create_model(param):
    model = cp_model.CpModel()

    chips = _make_chips(param, model)

    for pos in param.area:
        model.Add(_compute_sum_at_pos(chips, pos) <= 1)

    for stat, (lower_bound, upper_bound) in param.stats.items():
        sum_stat = _compute_sum_stat(chips, stat)
        if lower_bound is not None:
            model.Add(sum_stat >= lower_bound)
        if upper_bound is not None:
            model.Add(sum_stat <= upper_bound)

    return model, [(var, entry) for var, entry, _ in chips]

class _SolutionCallback(cp_model.CpSolverSolutionCallback):
    def __init__(self, chips, items, callback):
        super().__init__()
        self._chips = chips
        self._items = items
        self._callback = callback
        self._solutions = {}

    def on_solution_callback(self):
        """This is called by the solver"""
        try:
            self._on_solution_callback()
        except Exception as exc: # pylint: disable=broad-except
            print(exc, file=sys.stderr)
            self._callback((Command.ERROR, exc))

    def _on_solution_callback(self):
        sol = Solution([item for var, item in self._items
                        if self.SolutionBooleanValue(var.Index())])

        key = frozenset(sol.chip_ids())

        old_sol = self._solutions.get(key)
        if old_sol is None:
            self._solutions[key] = sol
            self._callback((Command.NEW, sol))
        elif sol.num_rotated() < old_sol.num_rotated():
            self._solutions[key] = sol
            self._callback((Command.REPLACE, old_sol, sol))

class LocalSolver:
    """A solver that computes in the current process."""

    def __init__(self, param):
        self.param = param
        self.started = self.cancelled = False
        self.lock = threading.Lock()
        self.callback = None

    def _do_compute(self, simple_queue):
        try:
            model, items = _create_model(self.param)
            solver = cp_model.CpSolver()
            callback = _SolutionCallback(self.param.chips, items,
                                         simple_queue.put)
            with self.lock:
                if self.cancelled:
                    return
                self.callback = callback
            solver.SearchForAllSolutions(model, callback)
        except Exception as exc: # pylint: disable=broad-except
            simple_queue.put((Command.ERROR, exc))
        finally:
            simple_queue.put((Command.DONE,))

    def run(self):
        """Finds all solutions."""
        with self.lock:
            if self.started:
                raise ValueError('solver already started')
            self.started = True

            if self.cancelled:
                return

            simple_queue = queue.SimpleQueue()
            thread = threading.Thread(target=self._do_compute,
                                      args=(simple_queue,),
                                      name="Computation Thread",
                                      daemon=True)
            thread.start()

        while True:
            obj = simple_queue.get()
            if obj[0] == Command.DONE:
                break
            yield obj

        thread.join()

    def cancel(self):
        """Stops the solver ASAP."""
        with self.lock:
            self.cancelled = True

            if self.callback is not None:
                self.callback.StopSearch()

class Solver:
    """A solver that computes in another process."""

    def __init__(self, param):
        self.param = param
        self.process = None
        self.started = self.cancelled = False
        self.lock = threading.Lock()

    @staticmethod
    def _do_compute(param, conn):
        try:
            model, items = _create_model(param)
            solver = cp_model.CpSolver()
            callback = _SolutionCallback(param.chips, items,
                                         conn.send)
            solver.SearchForAllSolutions(model, callback)
        except Exception as exc: # pylint: disable=broad-except
            conn.send((Command.ERROR, exc))

    def run(self):
        """Find all solutions"""

        with self.lock:
            if self.started:
                raise ValueError('solver already started')
            self.started = True

            if self.cancelled:
                return

            conn1, conn2 = mp.Pipe(False)
            self.process = mp.Process(target=Solver._do_compute,
                                      args=(self.param, conn2),
                                      name="Computation Process",
                                      daemon=True)
            self.process.start()
            conn2.close()

        try:
            while True:
                yield conn1.recv()
        except EOFError:
            pass

        self.process.join()

        with self.lock:
            self.process = None

    def cancel(self):
        """Cancel the solver; it'll stop ASAP"""
        with self.lock:
            self.cancelled = True

            if self.process is not None:
                self.process.terminate()

def process_solver_run(solver_run):
    """Processes Solver().run()."""

    sols = []
    for cmd in solver_run:
        if cmd[0] == Command.NEW:
            sols.append(cmd[1])
        elif cmd[0] == Command.REPLACE:
            sols.remove(cmd[1])
            sols.append(cmd[2])
        elif cmd[0] == Command.ERROR:
            raise RuntimeError('solver error') from cmd[1]
        else:
            raise RuntimeError(f'unexpected command {cmd[0]}')

    return sols

def find_all_solutions(param, *, solver_factory=Solver):
    """Finds all solutions."""

    return process_solver_run(solver_factory(param).run())
