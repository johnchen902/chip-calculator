#!/usr/bin/env python3
"""Read chips from images"""
from collections import Counter
import os.path

import cv2 as cv
import numpy as np

from .model import Shape, Chip

EXTENSIONS = [
    'bmp', 'dib', 'jpeg', 'jpg', 'jpe', 'jp2', 'png', 'webp',
    'pbm', 'pgm', 'ppm', 'pxm', 'pnm', 'pfm', 'sr', 'ras',
    'tiff', 'tif', 'exr', 'hdr', 'pic',
]

HOLES = [2, 0, 0, 0, 1, 0, 1, 0, 2, 1]

def imread(filename, flag=cv.IMREAD_COLOR):
    """cv2.imread but raise RuntimeError on error"""
    img = cv.imread(filename, flag)
    if img is None:
        raise RuntimeError(f'Failed to read image {filename}')
    return img

def _imread_icon(icon, flag=cv.IMREAD_COLOR):
    return imread(os.path.join('icons', f'{icon}.png'), flag)

class ChipFinder:
    """The algorithm to find chip in images"""
    _MIN_SQUARE_SIDE = 11
    _ICON_SIZE = 32
    _MIN_ICON_MATCH = 0.7
    _MAX_CHIP_ICON_DISTSQ = 10000
    _CHIP_ICON_DX = 3
    _CHIP_ICON_DY = 140
    _CHIP_W = 148
    _CHIP_H = 180
    _MIN_BLOCK_SIDE = 20
    _MAX_BLOCK_SIDE = 29
    _MIN_MASK_GRAY = 100
    _MAX_MASK_GRAY = 200
    _STRENTHEN_SIZE = 46
    _LOCK_W = 32
    _LOCK_H = 32
    _MIN_DIGIT_COLSUM = 3
    _ATTR_CHIP_DX = 36
    _ATTR_CHIP_DY = 8
    _ATTR_W = 36
    _ATTR_H = 28
    _SHORT_MASK_H = 110
    _TALL_MASK_H = 144

    # _find_obj_loc: location of obj is unknown, return location
    # _find_obj: location of obj is unknown, return obj
    # _read_obj: location of obj in known, return obj

    def __init__(self):
        self._icons = {}
        for stat in 'dpar':
            self._icons[stat] = _imread_icon(stat, cv.IMREAD_GRAYSCALE)

        self._digits = {}
        for i, holes in enumerate(HOLES):
            self._digits[i] = holes, _imread_icon(i, cv.IMREAD_GRAYSCALE)

    @staticmethod
    def _find_squares(img):
        """Find some squares on the image."""
        _, img = cv.threshold(img, 127, 255, cv.THRESH_BINARY)
        cons, _ = cv.findContours(img, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)[-2:]

        for con in cons:
            conlen = cv.arcLength(con, True)
            con = cv.approxPolyDP(con, 0.02 * conlen, True)
            if len(con) != 4:
                # not a quadrilaterals
                continue

            x, y, w, h = cv.boundingRect(con)
            if min(w, h) < ChipFinder._MIN_SQUARE_SIDE:
                # Too small
                continue
            if not -1 <= w - h <= 1:
                # bounding box not a square
                continue

            yield x, y, w

    def _find_some_icons(self, img):
        img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        for x, y, w in ChipFinder._find_squares(img):
            subimg = img[y : y + w, x : x + w]
            scale = ChipFinder._ICON_SIZE / (w - 1)
            subimg = cv.resize(subimg, (0, 0), fx=scale, fy=scale)

            for icon in self._icons.values():
                matches = cv.matchTemplate(icon, subimg, cv.TM_CCOEFF_NORMED)
                if matches.max() >= ChipFinder._MIN_ICON_MATCH:
                    yield x, y, w
                    break

    def _detect_icon_size(self, img):
        """Find length of sides of the icons on the image."""
        counter = Counter(w for _, _, w in self._find_some_icons(img))
        if not counter:
            raise ValueError('No icon detected')
        return counter.most_common(1)[0][0] - 1

    def _auto_resize(self, img):
        """Resize image automatically."""
        iconsz = self._detect_icon_size(img)
        scale = ChipFinder._ICON_SIZE / iconsz
        img = cv.resize(img, (0, 0), fx=scale, fy=scale)
        return img

    @staticmethod
    def _cc_argmax(score, comps, n):
        """Magic. I forgot this already."""
        best = [None] * n
        for i, j in np.transpose(comps.nonzero()):
            x = comps[i, j]
            if best[x] is None or score[i, j] > score[best[x]]:
                best[x] = i, j
        return best

    def _find_icons(self, img):
        """Find icons on the image"""
        img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        for label, icon in self._icons.items():
            match = cv.matchTemplate(icon, img, cv.TM_CCOEFF_NORMED)

            _, mat = cv.threshold(match, ChipFinder._MIN_ICON_MATCH,
                                  255, cv.THRESH_BINARY)
            n, comps = cv.connectedComponents(mat.astype(np.uint8))

            best = ChipFinder._cc_argmax(match, comps, n)

            for index in best[1:]:
                yield index, label

    @staticmethod
    def _is_icon_near(icon1, icon2):
        """Check if the icons are near enough to be on the same chip"""
        (y1, x1), _ = icon1
        (y2, x2), _ = icon2
        distsq = (y1 - y2) ** 2 + (x1 - x2) ** 2
        return distsq <= ChipFinder._MAX_CHIP_ICON_DISTSQ

    @staticmethod
    def _group_icons(icons):
        """Group icons by the chips they're on."""
        icons = list(icons)
        while icons:
            center = icons[0]
            group = [i for i in icons if ChipFinder._is_icon_near(i, center)]
            icons = [i for i in icons if not ChipFinder._is_icon_near(i, center)]
            yield group

    @staticmethod
    def _chips_loc_key(chiploc):
        (x, y, _, _), _ = chiploc
        return y + x // 10

    def _find_chips_loc(self, img):
        """Find all occurences of chip on the image."""
        chiplocs = []
        img_h, img_w = img.shape[:2]
        for group in ChipFinder._group_icons(self._find_icons(img)):
            x = min(x for (_, x), _ in group) - ChipFinder._CHIP_ICON_DX
            y = max(y for (y, _), _ in group) - ChipFinder._CHIP_ICON_DY
            w = ChipFinder._CHIP_W
            h = ChipFinder._CHIP_H

            if x >= 0 and x + w <= img_w and y >= 0 and y + h <= img_h:
                chiplocs.append(((x, y, w, h), group))
        chiplocs.sort(key=ChipFinder._chips_loc_key)
        return chiplocs

    @staticmethod
    def _mask_contour_key(img):
        x, y = img.shape
        x /= 2
        y /= 2
        def key(con):
            x1, y1, w, h = cv.boundingRect(con)
            x2, y2 = x1 + w, y1 + h
            return min(x - x1, x2 - x, y - y1, y2 - y)
        return key

    @staticmethod
    def _trig(img, n):
        h, w = img.shape[:2]
        return ([h - 1 - i for i in range(n) for j in range(n - i)],
                [w - 1 - j for i in range(n) for j in range(n - i)])

    @staticmethod
    def _contains_center(rect, imgshape):
        x, y, w, h = rect
        img_h, img_w = imgshape
        return x * 2 <= img_w <= (x + w) * 2 and y * 2 <= img_h <= (y + h) * 2

    @staticmethod
    def _is_uturn(p1, p2, p3):
        return np.inner(p2 - p1, p2 - p3) > 0

    @staticmethod
    def _get_block_size(con):
        def key(x):
            return np.minimum(con % x, -con % x).min(axis=1).max()
        return min(range(ChipFinder._MIN_BLOCK_SIDE,
                         ChipFinder._MAX_BLOCK_SIDE + 1), key=key)

    @staticmethod
    def _read_mask_outline(img):
        """Read outline of the mask on the image."""
        img = cv.bilateralFilter(img, 5, 75, 75)
        img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        img = cv.inRange(img, ChipFinder._MIN_MASK_GRAY, ChipFinder._MAX_MASK_GRAY)
        img[ChipFinder._trig(img, ChipFinder._STRENTHEN_SIZE)] = 0
        img[:ChipFinder._LOCK_H, -ChipFinder._LOCK_W:] = 0

        cons, _ = cv.findContours(img, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)[-2:]
        if not cons:
            return None

        con = max(cons, key=cv.contourArea)
        con = con.reshape((-1, 2))

        shift = con.min(axis=0)
        con -= shift

        bsz = ChipFinder._get_block_size(con)
        con = (con + bsz // 2) // bsz

        # remove duplicate adjacent points
        con = con[(con != np.roll(con, -1, axis=0)).any(axis=-1)]

        if not cv.contourArea(con):
            uturns = []
            for i, _ in enumerate(con):
                if ChipFinder._is_uturn(con[i - 2], con[i - 1], con[i]):
                    uturns.append((i - 1) % len(con))
            if uturns:
                con = con[min(uturns) : max(uturns) + 1]

        return con

    @staticmethod
    def _outline_to_mask(con):
        """Convert outline to mask"""
        if con is None or not con.size:
            return Shape(set())

        w, h = con.max(axis=0)
        grid = np.zeros((h, w))

        for x1, y1, x2, y2 in np.c_[con, np.roll(con, -1, axis=0)]:
            if y1 == y2 and y1 < h:
                if x1 < x2:
                    grid[y1, x1 : x2] += 1
                elif x1 > x2:
                    grid[y1, x2 : x1] -= 1

        grid = np.cumsum(grid, axis=0)
        return Shape([(r, c) for r, c in np.transpose(grid.nonzero())])

    @staticmethod
    def _read_mask(img):
        """Read the mask on the image"""
        con = ChipFinder._read_mask_outline(img)
        return ChipFinder._outline_to_mask(con)

    def _read_digit(self, img):
        """Read the digit on the image"""
        holes = cv.connectedComponents(~img)[0] - 2

        best = bestscore = None
        for digit, (expected_holes, template) in self._digits.items():
            if holes != expected_holes:
                continue
            score = cv.matchTemplate(template, img, cv.TM_SQDIFF_NORMED).min()

            if best is None or score < bestscore:
                best = digit
                bestscore = score

        return best

    def _read_int(self, img):
        """Read the integer on the image"""
        img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

        thre, _ = cv.threshold(img, 0, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)
        if thre >= 120:
            # voodoo but tests pass
            thre += 10
        _, img = cv.threshold(img, thre, 255, cv.THRESH_BINARY)

        colsum = img.sum(axis=0) // 255
        good_cols = np.where(colsum >= ChipFinder._MIN_DIGIT_COLSUM)[0]
        # group consecutive columns
        groups = np.split(good_cols, np.where(np.diff(good_cols) != 1)[0] + 1)

        ans = 0
        for colgroup in groups:
            digit_img = img.copy()
            digit_img[:, :colgroup[0]] = 0
            digit_img[:, colgroup[-1] + 1:] = 0

            digit = self._read_digit(digit_img)
            if digit is not None:
                ans = ans * 10 + digit

        if ans == 0:
            # should return a non-zero value
            return 1

        return ans

    def _read_attrs(self, img, group):
        """Read value of each attribute in group"""
        for (y, x), label in group:
            x1 = x + ChipFinder._ATTR_CHIP_DX
            y1 = y + ChipFinder._ATTR_CHIP_DY
            x2 = x1 + ChipFinder._ATTR_W
            y2 = y1 + ChipFinder._ATTR_H

            value = self._read_int(img[y1:y2, x1:x2])
            yield label, value

    def _find_chips(self, img, with_subimage):
        """Find chips on the image"""
        chips = []
        for (x, y, w, h), group in self._find_chips_loc(img):
            if len(group) > 2:
                h2 = ChipFinder._SHORT_MASK_H
            else:
                h2 = ChipFinder._TALL_MASK_H

            mask = ChipFinder._read_mask(img[y : y + h2, x : x + w])
            attrs = dict(self._read_attrs(img, group))

            chip = Chip(mask, **attrs)

            if with_subimage:
                chips.append((chip, img[y : y + h, x : x + w]))
            else:
                chips.append(chip)
        return chips

    def find(self, img, with_subimage=False):
        """Find all chips on the image"""
        try:
            img = self._auto_resize(img)
        except ValueError:
            return []

        return self._find_chips(img, with_subimage)
