Solver of a specific problem about Girls' Frontline, a mobile game.

# Features

* Cross-platform
* Supports HOCs below 5 stars
* Supports chips below 5 stars and of any shape
* Supports leaving some holes
* (TODO) Import chips from images
* (TODO) Import/exports chips from/to other formats

# Supported Platforms

I'm developing this on Linux.
The app should run on Windows and Mac too,
but expect some platform-specific bugs until I release this on NGA.

# Installation

1. Install PyGObject.
   (See https://pygobject.readthedocs.io/en/latest/getting_started.html)

2. Install GtkSourceView 4.
   (It's a dependency of gedit, so it's likely installed anyway.)

3. Install the remaining dependencies in requirement.txt.
   (The specified versions are just the ones I currently use.
    Feel free to ignore it as long as the app doesn't break.)

# Usage

* `python3 -m chipcalc` for CLI version
* `python3 -m chipcalc.gtk` for GTK+ version
